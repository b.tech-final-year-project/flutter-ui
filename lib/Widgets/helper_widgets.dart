import 'package:flutter/material.dart';

Widget loadingBuffer() {
  return Center(
    child: Image.asset(
      'assets/images/loading.png',
      color: Color.fromRGBO(255, 255, 255, 0.5),
      colorBlendMode: BlendMode.modulate,
    ),
    // child:  Text('data'),
  );
}
