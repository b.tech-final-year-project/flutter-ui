import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:toast/toast.dart';

import '../config.dart';

class CourseItem extends StatefulWidget {
  final dynamic course;
  final String token;
  CourseItem(this.token, this.course);
  @override
  _CourseItemState createState() => _CourseItemState();
}

class _CourseItemState extends State<CourseItem> {
  bool _isTraing = false;

  void _trainModel() async {
    Toast.show("Started Training", context, gravity: Toast.CENTER);
    setState(() {
      _isTraing = true;
    });
    var url = '$baseUrl/train_model/${widget.course["id"]}';
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": widget.token
    };

    String _body = '{"retrain" : 0}';

    var r = await post(Uri.encodeFull(url), headers: headers, body: _body);

    if (r.statusCode != 200) {
      Toast.show(jsonDecode(r.body)["message"], context);
    } else {
      Toast.show("Training Done", context, gravity: Toast.CENTER);
      setState(() {
        _isTraing = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Text(widget.course["name"]),
        ),
        _isTraing
            ? CircularProgressIndicator()
            : IconButton(icon: Icon(Icons.settings), onPressed: _trainModel)
      ],
    );
  }
}
