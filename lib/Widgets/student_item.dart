import 'package:attendance_app/Screens/admin/upload_photo_screen.dart';
import 'package:flutter/material.dart';

class StudentItem extends StatefulWidget {
  final dynamic student;
  final String token;
  final Function delete;
  StudentItem(this.token, this.student, this.delete);
  @override
  _StudentItemState createState() => _StudentItemState();
}

class _StudentItemState extends State<StudentItem> {
  bool _isDeleting = false;

  @override
  Widget build(BuildContext context) {
    final primary = Theme.of(context).primaryColor;
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(13),
        color: Colors.white,
      ),
      width: double.infinity,
      height: 69,
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      widget.student["name"],
                      style: TextStyle(
                          color: primary,
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                  icon: Icon(Icons.photo_camera),
                  onPressed: () => Navigator.of(context).pushNamed(
                    UploadPhotoStudent.routeName,
                    arguments: widget.student,
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _isDeleting
                    ? CircularProgressIndicator()
                    : IconButton(
                        icon: Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                        // onPressed: () => widget.delete(widget.student["regno"]),
                        onPressed: () {
                          AlertDialog alert = AlertDialog(
                            title: Text("Confirm delete"),
                            content: Text(
                                "You're deleting : ${(widget.student["name"])}"),
                            actions: [
                              RaisedButton(
                                color: Colors.green,
                                child: Text("Confirm"),
                                onPressed: () {
                                  widget.delete(widget.student["regno"]);
                                  Navigator.of(context).pop();
                                },
                              ),
                              RaisedButton(
                                color: Colors.red,
                                child: Text("Cancel"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              )
                            ],
                          );

                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return alert;
                            },
                          );
                        },
                      ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
