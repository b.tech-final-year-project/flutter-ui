import 'package:flutter/material.dart';

typedef void VoidCallback(int x, int y, int z);

class TicketItemU extends StatelessWidget {
  final dynamic ticket;
  final String token;
  final int status;
  final int index;
  final VoidCallback callback;
  TicketItemU(this.token, this.ticket, this.status, this.index, this.callback);
  final bool _isMarking = false;

  @override
  Widget build(BuildContext context) {
    return !(status == 0)
        ? SizedBox()
        : Card(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(ticket['issue']),
                        Divider(),
                        Text(ticket['course_name']),
                        Text(
                          ticket['instructor_name'],
                          style: TextStyle(color: Colors.grey),
                        ),
                        Text(
                          ticket['created_at'],
                          style: TextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                  ),
                ),
                _isMarking
                    ? CircularProgressIndicator()
                    : IconButton(
                        icon: Icon(
                          Icons.assignment_turned_in,
                          color: Colors.grey,
                        ),
                        onPressed: () {
                          callback(1, index, ticket['ticket_id']);
                        })
              ],
            ),
          );
  }
}
