import 'package:flutter/material.dart';

import '../Screens/admin/manage_facutly_screen.dart';

class FacultyItem extends StatefulWidget {
  final dynamic faculty;
  final String token;
  final Function delete;
  FacultyItem(this.token, this.faculty, this.delete);
  @override
  _FacultyItemState createState() => _FacultyItemState();
}

class _FacultyItemState extends State<FacultyItem> {
  bool _isDeleting = false;
  bool isAdmin = false;

  @override
  Widget build(BuildContext context) {
    final primary = Theme.of(context).primaryColor;
    return GestureDetector(
      onTap: () => Navigator.of(context)
          .pushNamed(ManageFaculty.routeName, arguments: widget.faculty),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
        ),
        width: double.infinity,
        height: 75,
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        widget.faculty["name"],
                        style: TextStyle(
                            color: primary,
                            fontWeight: FontWeight.bold,
                            fontSize: 18),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(right: 15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _isDeleting
                      ? CircularProgressIndicator()
                      : IconButton(
                          icon: Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                          onPressed: () {
                            setState(() {
                              _isDeleting = true;
                            });
                            widget.delete(widget.faculty["public_id"]);
                            setState(() {
                              _isDeleting = false;
                            });
                          },
                        ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
