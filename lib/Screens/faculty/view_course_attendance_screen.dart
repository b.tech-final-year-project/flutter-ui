import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';
import 'package:toast/toast.dart';

import 'attendance_list_screen.dart';
import '../../config.dart';

class CourseAttendance extends StatefulWidget {
  static final routeName = '/view-course-attendance';
  @override
  _CourseAttendanceState createState() => _CourseAttendanceState();
}

class _CourseAttendanceState extends State<CourseAttendance> {
  bool _isLoadingInitData = true;
  dynamic currentCourse;
  String slotId;
  String token;
  dynamic attendanceData = [];
  List<dynamic> absentList = [];
  List<dynamic> presentList = [];

  void loadInitData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('token');
    var routeArgs = ModalRoute.of(context).settings.arguments as Map;
    setState(() {
      currentCourse = routeArgs['currentCourse'];
      slotId = routeArgs['slotId'];
      token = stringValue;
    });

    var url = Uri.parse("$baseUrl/attendance/view");

    MultipartRequest request = MultipartRequest("POST", url);
    request.fields["id"] = currentCourse.id.toString();
    request.fields["slot_id"] = slotId;
    request.headers["x-access-token"] = token;

    try {
      final streamedResponse = await request.send();
      final response = await Response.fromStream(streamedResponse);
      if (response.statusCode == 200) {
        print("Success");
        var jsonData = jsonDecode(response.body);
        print("JSON DATA" + jsonData + "\n");
        if (jsonData["Status"] == "Success") {
          setState(() {
            attendanceData = jsonData["Class Attendance"];
            print("Attendance Data : " + attendanceData + "\n");
          });
        }
      } else {
        // print(response.body);
      }
    } on Exception catch (_) {}

    attendanceData.forEach((e) {
      if (e["present"]) {
        setState(() {
          presentList.add(e);
        });
      } else {
        setState(() {
          absentList.add(e);
        });
      }
    });

    setState(() {
      _isLoadingInitData = false;
    });
  }

  @override
  void initState() {
    loadInitData();
    super.initState();
  }

  void discardAttendance() async {
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    var url = "$baseUrl/deleteattendance/$slotId";
    var r = await delete(Uri.encodeFull(url), headers: headers);
    Toast.show("Attendance Discarded", context, gravity: Toast.CENTER);
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Attendance'),
      ),
      body: _isLoadingInitData
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: EdgeInsets.all(8),
              child: Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushNamed(
                          AttendanceListScreen.routeName,
                          arguments: {
                            'title': 'Present List',
                            'data': presentList,
                          });
                    },
                    child: Container(
                      width: double.infinity,
                      child: Card(
                        elevation: 3,
                        child: Padding(
                          padding: EdgeInsets.all(16),
                          child:
                              Text('View Present List (${presentList.length})'),
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushNamed(
                          AttendanceListScreen.routeName,
                          arguments: {
                            'title': 'Absentee List',
                            'data': absentList,
                          });
                    },
                    child: Container(
                      width: double.infinity,
                      child: Card(
                        elevation: 3,
                        child: Padding(
                          padding: EdgeInsets.all(16),
                          child:
                              Text('View Absent List (${absentList.length})'),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: discardAttendance,
        backgroundColor: Colors.red,
        icon: Icon(Icons.delete),
        label: Text('Discard'),
      ),
    );
  }
}
