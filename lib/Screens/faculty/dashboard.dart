import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Auth/google_sign_in.dart';
import '../../Widgets/main_drawer.dart';
import '../../Widgets/main_screen_link.dart';
import 'create_course_screen.dart';
// import 'add_attendance_screen.dart';
import 'fac_view_raised_tickets.dart';
import 'manage_courses_screen.dart';
import 'add_attendance_screen_multi.dart';
import '../settings_screen.dart';

class Dashboard extends StatefulWidget {
  static final routeName = '/dashboard';
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  String token = '';
  String currentUser = '';
  var links = [
    {
      'title': CreateCourseScreen.title,
      'icon': Icons.add_circle_outline,
      'route': CreateCourseScreen.routeName,
    },
    {
      'title': ManageCoursesScreen.title,
      'icon': Icons.group,
      'route': ManageCoursesScreen.routeName,
    },
    {
      'title': 'Take Attendance',
      'icon': Icons.camera,
      'route': AddAttendanceScreenMulti.routeName,
    },
    {
      'title': 'View Tickets',
      'icon': Icons.assistant_photo,
      'route': ViewFacTickets.routeName,
    },
  ];

  void logout() async {
    final prefrences = await SharedPreferences.getInstance();
    await prefrences.clear();
    signOutGoogle();
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  void initState() {
    // getCurrentUserData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Attendance App'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.settings),
              onPressed: () =>
                  Navigator.of(context).pushNamed(SettingsScreen.routeName)),
          IconButton(icon: Icon(Icons.power_settings_new), onPressed: logout)
        ],
      ),
      drawer: MainDrawer(currentUser),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return MainScreenLink(links[index]['title'], links[index]['route'],
              links[index]['icon']);
        },
        itemCount: links.length,
      ),
    );
  }
}
