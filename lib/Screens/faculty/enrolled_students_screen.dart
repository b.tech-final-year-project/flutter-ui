import 'dart:convert';

import 'package:attendance_app/Screens/faculty/fac_student_profile_screen.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import '../../config.dart';

class EnrolledStudetsScreen extends StatefulWidget {
  static final routeName = '/enrolled-students-screen';
  static final title = 'View Enrolled Students';
  @override
  _EnrolledStudetsScreenState createState() => _EnrolledStudetsScreenState();
}

class _EnrolledStudetsScreenState extends State<EnrolledStudetsScreen> {
  var _loadedInitData = false;
  bool noStudentsEnrolled = false;
  dynamic enrolledStudents;
  dynamic currentCourse;
  String token;
  bool _raisingTicket = false;

  @override
  void didChangeDependencies() async {
    if (_loadedInitData == false) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String stringValue = prefs.getString('token');
      setState(() {
        token = stringValue;
      });
      final routeArgs = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        currentCourse = routeArgs['currentCourse'];
      });
      String url = '$baseUrl/enroll_students/${currentCourse.id}';
      var r = await http.get(
        url,
        headers: {
          'x-access-token': token,
          'Accept': 'Application/json',
        },
      );
      var jsonData = jsonDecode(r.body);
      setState(() {
        enrolledStudents = jsonData['students'];
      });
      print(enrolledStudents);
      if (enrolledStudents == null) {
        setState(() {
          noStudentsEnrolled = true;
        });
      }
      _loadedInitData = false;
    }
    super.didChangeDependencies();
  }

  void _deleteStudent(String regno, int id) async {
    String dropStudentURL = "$baseUrl/enroll_students/$id/$regno";
    var response = http.delete(
      Uri.encodeFull(dropStudentURL),
      headers: {
        'x-access-token': token,
        'Accept': 'Application/json',
      },
    );
  }

  void raiseTicket() async {
    setState(() {
      _raisingTicket = true;
    });
    var ticketUrl = '$baseUrl/enrolling_done/${currentCourse.id}';
    var r = await http.get(
      ticketUrl,
      headers: {
        'x-access-token': token,
        'Accept': 'Application/json',
      },
    );
    if (r.statusCode == 200) {
      var jsonData = jsonDecode(r.body);
      Toast.show(jsonData["message"], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
    }
    setState(() {
      _raisingTicket = false;
    });
  }

  Widget printStudent(dynamic student) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              child: InkWell(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      student["name"],
                      style: TextStyle(fontSize: 16),
                    ),
                    Text(
                      student['regno'],
                      style: TextStyle(color: Colors.grey),
                    )
                  ],
                ),
                onTap: () => Navigator.of(context)
                    .pushNamed(FacultyStudentProfile.routeName, arguments: {
                  "student": student,
                  "currentCourse": currentCourse
                }),
              ),
            ),
            IconButton(
              onPressed: () {
                _deleteStudent(student["regno"], currentCourse.id);
                setState(() {
                  enrolledStudents
                      .removeWhere((s) => s['regno'] == student["regno"]);
                });
              },
              icon: Icon(Icons.delete),
              color: Colors.redAccent,
            ),
          ],
        ),
        Divider(),
      ],
    );
  }

  Widget printList() {
    return noStudentsEnrolled
        ? Center(
            child: Text(
              'Such an empty space. Enroll Some students',
              style: TextStyle(color: Colors.grey),
            ),
          )
        : ListView.builder(
            itemBuilder: (ctx, index) {
              return printStudent(enrolledStudents[index]);
            },
            itemCount: enrolledStudents == null ? 0 : enrolledStudents.length,
          );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(EnrolledStudetsScreen.title),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: _loadedInitData
            ? Center(
                child: Text(
                  'loading data',
                  style: TextStyle(color: Colors.grey),
                ),
              )
            : printList(),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: raiseTicket,
        icon: Icon(Icons.assistant_photo),
        label: Text('Complete Enrollment'),
      ),
    );
  }
}
