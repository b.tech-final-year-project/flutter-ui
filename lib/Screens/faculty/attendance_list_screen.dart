import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';
import 'package:toast/toast.dart';

import '../../config.dart';

class AttendanceListScreen extends StatefulWidget {
  static final routeName = '/attendance-list-screen';

  @override
  _AttendanceListScreenState createState() => _AttendanceListScreenState();
}

class _AttendanceListScreenState extends State<AttendanceListScreen> {
  String title = '';
  bool isAbsentList = true;
  String token;
  dynamic routeData;

  List<dynamic> _list = [];
  List<dynamic> _names = [];

  @override
  void didChangeDependencies() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
    });
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    setState(() {
      title = routeArgs['title'];
      _list = routeArgs['data'];
      _names = routeArgs['names'];
    });
    if (title == "Present List") {
      setState(() {
        isAbsentList = false;
      });
    }
    super.didChangeDependencies();
  }

  void changeAttendance(item) async {
    String url = "$baseUrl/attendance/${item["course_id"]}/${item["slot_id"]}";
    int present = 1;
    if (title == "Present List") {
      present = 0;
    }
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    String _body =
        '{"student_id":"${item["student_id"]}","present":"$present"}';
    print(_body);
    var r = await put(Uri.encodeFull(url), headers: headers, body: _body);
    if (r.statusCode == 200) {
      var jsonData = jsonDecode(r.body);
      if (jsonData["Status"] == "Success") {
        Toast.show(jsonData["message"], context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.CENTER);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView.builder(
          itemBuilder: (ctx, index) {
            return Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text("${_names[index]} : ${_list[index]}"),
                      //"${_list[index]['regno']} ${_list[index]['name']}"),
                    ),
                    isAbsentList
                        ? IconButton(
                            icon: Icon(
                              Icons.close,
                              color: Colors.red,
                            ),
                          )
                        // onPressed: () => changeAttendance(_list[index]))
                        : IconButton(
                            icon: Icon(
                              Icons.done,
                              color: Colors.green,
                            ),
                          )
                    // onPressed: () => changeAttendance(_list[index])), //TO-DO
                  ],
                ),
                Divider(),
              ],
            );
          },
          itemCount: _list.length,
        ),
      ),
    );
  }
}
