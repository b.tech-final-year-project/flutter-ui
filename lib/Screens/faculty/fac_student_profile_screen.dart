import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../config.dart';

class FacultyStudentProfile extends StatefulWidget {
  static final routeName = '/faculty-student-profile-screen';
  @override
  _FacultyStudentProfileState createState() => _FacultyStudentProfileState();
}

class _FacultyStudentProfileState extends State<FacultyStudentProfile> {
  bool _isLoadingInitData = true;
  dynamic student;
  String token;
  dynamic currentCourse;
  String title = "Student Name";
  int totalClasses = 0;
  int presentCount = 0;
  dynamic presentDates = [];
  dynamic absentDates = [];

  @override
  void didChangeDependencies() async {
    if (_isLoadingInitData) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String stringValue = prefs.getString('token');
      setState(() {
        token = stringValue;
      });
      final routeArgs = ModalRoute.of(context).settings.arguments as dynamic;
      setState(() {
        student = routeArgs["student"];
        title = student["name"];
        currentCourse = routeArgs["currentCourse"];
        _isLoadingInitData = false;
      });
      loadInitData();
    }
    super.didChangeDependencies();
  }

  void loadInitData() async {
    Uri url = Uri.parse("$baseUrl/attendance/statistics");
    Map<String, String> headers = {
      'x-access-token': token,
      'Accept': 'Application/json',
    };
    MultipartRequest request = MultipartRequest("POST", url);
    request.fields["regno"] = student["regno"];
    request.fields["id"] = currentCourse.id.toString();
    request.headers["x-access-token"] = token;
    try {
      final streamedResponse = await request.send();
      final response = await Response.fromStream(streamedResponse);
      print(response.statusCode);
      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body);
        print(jsonData);
        if (jsonData["Status"] == "Success") {
          setState(() {
            totalClasses = jsonData["total_count"];
            presentCount = jsonData["present_count"];
            presentDates = jsonData["present_dates"];
            absentDates = jsonData["absent_dates"];
          });
        }
      } else {
        print("Error");
      }
    } on Exception catch (_) {
      print('object');
    }
    print(absentDates);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: _isLoadingInitData
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(8),
                child: Column(
                  children: <Widget>[
                    Card(
                      elevation: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Name : ${student["name"]}",
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                          Divider(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Roll No : ${student["regno"]}",
                              style: TextStyle(fontSize: 16),
                            ),
                          )
                        ],
                      ),
                    ),
                    Card(
                      elevation: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Center(
                              child: Text(
                                "Attendance Statistics",
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          Divider(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Total Classes : $totalClasses",
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Classes Present: $presentCount",
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Classes Absent: ${totalClasses - presentCount}",
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Card(
                      elevation: 2,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Absent Dates',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Divider(),
                          Container(
                            padding: EdgeInsets.all(8),
                            // height: 40.0 * (totalClasses - presentCount),
                            child: ListView.builder(
                              shrinkWrap: true,
                              itemBuilder: (ctx, index) {
                                return Text(
                                  "${absentDates[index].split('_')[0]} : ${absentDates[index].split('_')[1]}",
                                  style: TextStyle(fontSize: 16),
                                );
                              },
                              itemCount: absentDates.length,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
    );
  }
}
