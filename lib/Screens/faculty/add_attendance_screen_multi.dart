import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';

import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Models/Course.dart';
import '../../config.dart';
import 'attendance_result_screen.dart';

class AddAttendanceScreenMulti extends StatefulWidget {
  static final routeName = '/add-attendance-screen-multi';

  @override
  _AddAttendanceScreenMultiState createState() =>
      _AddAttendanceScreenMultiState();
}

class _AddAttendanceScreenMultiState extends State<AddAttendanceScreenMulti> {
  bool _isImageLoaded = false;
  bool _isUpdatingAttendance = false;
  List<Course> courseList = [];
  String token = '';
  Course selectedCourse;
  List<DropdownMenuItem<Course>> _dropdownMenuItems = [];
  List<Asset> images = List<Asset>();
  String _error;
  Uri uri = Uri.parse('$baseUrl/attendance');

  Future<String> _uploadMultipleImage() async {
    setState(() {
      _isUpdatingAttendance = true;
    });
    MultipartRequest request = http.MultipartRequest("POST", uri);

    var iter = 0;
    for (Asset image in images) {
      ByteData byteData = await image.getByteData();
      List<int> imageData = byteData.buffer.asUint8List();

      MultipartFile multipartFile = MultipartFile.fromBytes(
        'file',
        imageData,
        filename: 'img$iter.jpg',
        contentType: MediaType("image", "jpg"),
      );

// add file to multipart
      request.files.add(multipartFile);
      request.fields['id'] = selectedCourse.id.toString();
      request.headers["x-access-token"] = token;
      iter += 1;
    }
// send
    try {
      final streamedResponse = await request.send();
      final response = await http.Response.fromStream(streamedResponse);
      if (response.statusCode != 200) {
        return null;
      }
      final responseData = json.decode(response.body);
      print(json.decode(response.body));
      final absentList = responseData['absent_list'];
      final absentCount = responseData['absentCount'];
      final presentList = responseData['present_list'];
      final presentCount = responseData['presentCount'];
      final absentNames = responseData['absent_names'];
      final presentNames = responseData['present_names'];

      Navigator.of(context)
          .pushNamed(AttendanceResultScreen.routeName, arguments: {
        'absentList': absentList,
        'absentCount': absentCount,
        'presentList': presentList,
        'presentCount': presentCount,
        'presentNames': presentNames,
        'absentNames': absentNames,
        'slot_id': responseData['slot_id'],
      });
    } on Exception catch (_) {
      setState(() {
        _error = "error uploading file";
      });
    }
    setState(() {
      _isUpdatingAttendance = false;
    });
  }

  Future<String> _loadCourseList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('token');
    setState(() {
      token = stringValue;
    });
    var response = await http.get(
      Uri.encodeFull(baseUrl + '/courses'),
      headers: {
        'x-access-token': token,
        'Accept': 'Application/json',
      },
    );
    var data = jsonDecode(response.body);
    var courses = data['courses'];
    if (courses != null) {
      for (int i = 0; i < courses.length; i++) {
        var course = courses[i];
        var newCourse = new Course(
          id: course['id'],
          code: course['code'],
          name: course['name'],
          instructorId: course['instructor_id'],
          academicSession: course['academic_session'],
        );
        setState(() {
          courseList.add(newCourse);
          _dropdownMenuItems = buildDropdownMenuItems(courseList);
          selectedCourse = _dropdownMenuItems[0].value;
        });
      }
    }
  }

  List<DropdownMenuItem<Course>> buildDropdownMenuItems(List courses) {
    List<DropdownMenuItem<Course>> items = List();
    for (Course course in courses) {
      items.add(
        DropdownMenuItem(
          value: course,
          child: Text(course.name),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(Course courseSelected) {
    print("${courseSelected.name} Selected");
    setState(() {
      selectedCourse = courseSelected;
    });
  }

  @override
  void initState() {
    _loadCourseList();
    super.initState();
  }

  Widget buildGridView() {
    if (images != null)
      return GridView.count(
        crossAxisCount: 3,
        children: List.generate(images.length, (index) {
          Asset asset = images[index];
          return AssetThumb(
            asset: asset,
            width: 300,
            height: 300,
          );
        }),
      );
    else
      return Container(color: Colors.white);
  }

  Future<void> loadAssets() async {
    setState(() {
      images = List<Asset>();
    });

    List<Asset> resultList;
    String error;

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images = resultList;
      if (error == null) _error = 'No Error Dectected';
      _isImageLoaded = true;
    });
  }

  Widget _printSubmitButton() {
    if (_isImageLoaded) {
      return _isUpdatingAttendance
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: FlatButton(
                height: 50,
                color: Colors.green[300],
                textColor: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                onPressed: _uploadMultipleImage,
                child: Text('Update Attendance'),
              ),
            );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Register Attendance'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Text('Select Course'),
          SizedBox(
            height: 10,
          ),
          DropdownButton(
            value: selectedCourse,
            items: _dropdownMenuItems,
            onChanged: onChangeDropdownItem,
          ),
          // Center(child: Text('Error: $_error')),
          FlatButton(
            height: 50,
            color: Colors.green[300],
            textColor: Colors.white,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: Text("Select images"),
            onPressed: loadAssets,
          ),
          _printSubmitButton(),
          Expanded(
            child: buildGridView(),
          )
        ],
      ),
    );
  }
}
