import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../config.dart';
import 'view_course_attendance_screen.dart';

class PreviousAttendance extends StatefulWidget {
  static final routeName = '/view-previous-attendance';
  @override
  _PreviousAttendanceState createState() => _PreviousAttendanceState();
}

class _PreviousAttendanceState extends State<PreviousAttendance> {
  bool _isLoadingInitData = true;
  String token;
  dynamic currentCourse;
  dynamic prevAttendances = [];

  void loadInitData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('token');
    var routeArgs = ModalRoute.of(context).settings.arguments as Map;
    setState(() {
      currentCourse = routeArgs['currentCourse'];
      token = stringValue;
    });

    String url = "$baseUrl/attendance/view/${currentCourse.id}";
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };

    var r = await get(Uri.encodeFull(url), headers: headers);

    if (r.statusCode == 200) {
      var jsonData = jsonDecode(r.body);
      setState(() {
        prevAttendances = jsonData["Class Dates"];
      });
    }

    setState(() {
      _isLoadingInitData = false;
    });
  }

  @override
  void initState() {
    loadInitData();
    super.initState();
  }

  Widget prevAttendanceItem(dynamic item) {
    return GestureDetector(
      onTap: () => Navigator.of(context).pushNamed(CourseAttendance.routeName,
          arguments: {
            "currentCourse": currentCourse,
            "slotId": item["slot_id"]
          }),
      child: Card(
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Text(
            "${item["date"]}",
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Previous Attendance'),
      ),
      body: _isLoadingInitData
          ? Center(
              child: CircularProgressIndicator(),
            )
          : prevAttendances.length == 0
              ? Center(
                  child: Text(
                    'You\'ve not taken any attendences',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.grey,
                    ),
                  ),
                )
              : Padding(
                  child: ListView.builder(
                    itemBuilder: (ctx, index) {
                      print(index);
                      return prevAttendanceItem(prevAttendances[index]);
                    },
                    itemCount: prevAttendances.length,
                  ),
                  padding: EdgeInsets.all(8),
                ),
    );
  }
}
