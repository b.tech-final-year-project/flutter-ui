import 'dart:convert';

import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

import '../../Models/Course.dart';
import '../../config.dart';
import 'manage_course_screen.dart';

class CreateCourseScreen extends StatefulWidget {
  static final routeName = '/create-course';
  static final title = 'Create Course';

  @override
  _CreateCourseScreenState createState() => _CreateCourseScreenState();
}

class _CreateCourseScreenState extends State<CreateCourseScreen> {
  final _courseCodeController = TextEditingController();
  final _courseNameController = TextEditingController();
  String academicSession = "Winter";

  Future<String> validateCreateCourse() async {
    String token = '';
    final url = "$baseUrl/courses";
    final courseCode = _courseCodeController.text;
    final courseName = _courseNameController.text;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('token');
    setState(() {
      token = stringValue;
    });
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    String _body =
        '{"code":"${courseCode.split(' ').join()}","name":"$courseName","academic_session":"$academicSession"}';
    // print(_body);
    Response response = await post(url, headers: headers, body: _body);
    var data = jsonDecode(response.body);
    int statusCode = response.statusCode;
    print(data);
    var id = data['id'];
    if (statusCode == 200) {
      Toast.show("Course Added", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.TOP);
    }

    response = await http.get(
      Uri.encodeFull(baseUrl + '/courses/${id}'),
      headers: {
        'x-access-token': token,
        'Accept': 'Application/json',
      },
    );
    data = jsonDecode(response.body);
    var course = data['course'];
    Course createdCourse = Course(
      id: course['id'],
      code: course['code'],
      name: course['name'],
      instructorId: course['instructor_id'],
      academicSession: course['academic_session'],
    );
    print(data);
    Navigator.of(context).pushReplacementNamed(
      ManageCourseScreen.routeName,
      arguments: createdCourse,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(CreateCourseScreen.title),
      ),
      // drawer: MainDrawer('Test'),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              TextFormField(
                controller: _courseCodeController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Course Code',
                  prefixIcon: Icon(Icons.lock_outline),
                  labelStyle: TextStyle(fontSize: 15),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: _courseNameController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Course Name',
                  prefixIcon: Icon(Icons.lock_outline),
                  labelStyle: TextStyle(fontSize: 15),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                child: Container(
                  width: double.infinity,
                  margin: EdgeInsets.symmetric(vertical: 10),
                  child: DropdownButton<String>(
                    value: academicSession,
                    icon: Icon(Icons.arrow_downward),
                    iconSize: 24,
                    elevation: 16,
                    onChanged: (String newValue) {
                      setState(() {
                        academicSession = newValue;
                      });
                    },
                    items: <String>[
                      'Summer',
                      'Winter',
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ),
              ),
              RaisedButton(
                onPressed: validateCreateCourse,
                child: Text('Create Course'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
