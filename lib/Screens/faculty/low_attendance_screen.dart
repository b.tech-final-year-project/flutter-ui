import 'package:flutter/material.dart';

class LowAttendanceScreen extends StatefulWidget {
  static final routeName = '/low-attendance-screen';
  @override
  _LowAttendanceScreenState createState() => _LowAttendanceScreenState();
}

class _LowAttendanceScreenState extends State<LowAttendanceScreen> {
  bool _isLoadingInitData = true;
  dynamic studentList = [];

  @override
  void didChangeDependencies() {
    if (_isLoadingInitData == true) {
      final routeArgs = ModalRoute.of(context).settings.arguments;
      setState(() {
        studentList = routeArgs;
        _isLoadingInitData = false;
      });
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Student with low attendance'),
      ),
      body: _isLoadingInitData
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: EdgeInsets.all(8),
              child: studentList == null
                  ? Center(
                      child: Text(
                        'No student with low attendance',
                        style: TextStyle(fontSize: 16, color: Colors.grey),
                      ),
                    )
                  : ListView.builder(
                      itemBuilder: (ctx, index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(studentList[index]),
                              Divider(),
                            ],
                          ),
                        );
                      },
                      itemCount: studentList.length,
                    ),
            ),
    );
  }
}
