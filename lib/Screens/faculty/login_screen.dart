import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import './dashboard.dart';
import '../../Auth/google_sign_in.dart';
import '../../Widgets/sign_in_button.dart';

class LoginScreen extends StatefulWidget {
  static final routeName = '/fac_login';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  Future<void> promiseFn() async {
    var response = await signInWithGoogle();
    var result = jsonDecode(response);
    // Fix needed : Need to check the response status and authenticate
    // instead of directly checking if it's NULL.
    if (result != null) {
      // print(result["data"]);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      setState(() {
        prefs.setString("token", result["idToken"]);
        prefs.setBool("admin", false);
      });
      // Navigate to student dashboard after authenticating
      Navigator.of(context).pushReplacementNamed(Dashboard.routeName);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image(
                  image: AssetImage("assets/images/nit_logo_black.png"),
                  height: 200.0),
              SizedBox(height: 50),
              SignInButton(promiseFn)
            ],
          ),
        ),
      ),
    );
  }
}
