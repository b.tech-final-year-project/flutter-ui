import 'dart:convert';

import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

import '../../Widgets/helper_widgets.dart';
import '../../config.dart';
import 'enrolled_students_screen.dart';

class EnrollStudentsScreen extends StatefulWidget {
  static final routeName = '/enroll-students';
  @override
  _EnrollStudentsScreenState createState() => _EnrollStudentsScreenState();
}

class _EnrollStudentsScreenState extends State<EnrollStudentsScreen> {
  var _loadedInitData = false;
  bool _isEnrolling = false;
  bool _value1 = false;
  final String url = '$baseUrl/enroll_students';
  String token = '';
  dynamic currentCourse;
  dynamic studentsToEnroll;
  dynamic enrolledStudents;
  List<String> enrolledRollNo = [];
  int _studentsToEnrollLength = 0;
  List<String> toEnroll = [];

  @override
  void didChangeDependencies() {
    if (!_loadedInitData) {
      var routeArgs = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        currentCourse = routeArgs['currentCourse'];
        enrolledStudents = routeArgs['enrolledStudents'];
      });
      if (enrolledStudents != null) {
        enrolledStudents.forEach((enrolledStudent) {
          enrolledRollNo.add(enrolledStudent['regno']);
        });
      }
      setState(() {
        enrolledRollNo = enrolledRollNo;
      });
      _loadedInitData = true;
      _loadStudentsToEnroll();
    }
    super.didChangeDependencies();
  }

  void _loadStudentsToEnroll() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('token');
    setState(() {
      token = stringValue;
    });

    var url = "$baseUrl/students";

    var response = await http.get(Uri.encodeFull(url), headers: {
      'x-access-token': token,
    });

    var reqData = jsonDecode(response.body);
    var tempList = reqData['students'];
    tempList.forEach((student) {
      if (enrolledRollNo.contains(student['regno'])) {
        student['isEnrolled'] = true;
        student['isAlreadyEnrolled'] = true;
      } else {
        student['isAlreadyEnrolled'] = false;
        student['isEnrolled'] = false;
      }
    });
    setState(() {
      studentsToEnroll = tempList;
      _studentsToEnrollLength = studentsToEnroll.length;
    });
  }

  void enrollStudents() async {
    setState(() {
      _isEnrolling = true;
    });
    final enrollUrl = "$baseUrl/enroll_students";
    String rollNoToEnroll = '[';
    studentsToEnroll.forEach((student) {
      if (student['isEnrolled']) {
        rollNoToEnroll = '$rollNoToEnroll"${student['regno']}",';
      }
    });
    rollNoToEnroll =
        '${rollNoToEnroll.substring(0, rollNoToEnroll.length - 1)}]';
    var req_body = '{"id" : ${currentCourse.id},"regno" : $rollNoToEnroll}';
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    String _body = '{"id":${currentCourse.id},"regno" : $rollNoToEnroll}';
    print("rno" + rollNoToEnroll);
    var response = await http.post(Uri.encodeFull(enrollUrl),
        headers: headers, body: _body);
    if (response.statusCode == 200) {
      Toast.show(jsonDecode(response.body)['message'], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      Navigator.of(context)
          .pushReplacementNamed(EnrolledStudetsScreen.routeName, arguments: {
        'currentCourse': currentCourse,
      });
    }
    setState(() {
      _isEnrolling = false;
    });
  }

  void _value1Changed(bool value) {
    setState(() {
      _value1 = value;
    });
    print(_value1);
  }

  Widget printList() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
        child: Column(
          children: <Widget>[
            _isEnrolling
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Container(
                    margin: EdgeInsets.symmetric(vertical: 5),
                    child: FlatButton(
                      height: 50,
                      color: Colors.green[300],
                      textColor: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      onPressed: enrollStudents,
                      child: Text('Enroll Students'),
                    ),
                  ),
            Container(
              height: (MediaQuery.of(context).size.height) * 0.8,
              child: ListView.builder(
                itemBuilder: (ctx, index) {
                  return studentsToEnroll[index]['isAlreadyEnrolled']
                      ? SizedBox()
                      : CheckboxListTile(
                          value: studentsToEnroll[index]['isEnrolled'],
                          onChanged: (bool value) {
                            setState(() {
                              studentsToEnroll[index]['isEnrolled'] = value;
                            });
                          },
                          title: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                studentsToEnroll[index]['name'],
                              ),
                              Text(
                                studentsToEnroll[index]['regno'],
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 14,
                                ),
                              ),
                            ],
                          ),
                          controlAffinity: ListTileControlAffinity.leading,
                          activeColor: Colors.green,
                        );
                  // return Checkbox(value: _value1, onChanged: _value1Changed);
                  // return Text(studentsToEnroll[index]['name']);
                },
                itemCount: _studentsToEnrollLength,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Enroll Students'),
      ),
      body: _loadedInitData ? printList() : loadingBuffer(),
    );
  }
}
