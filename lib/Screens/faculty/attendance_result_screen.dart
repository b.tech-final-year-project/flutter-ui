import 'package:attendance_app/Screens/faculty/known_faces_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';
import 'package:toast/toast.dart';

import 'attendance_list_screen.dart';
import 'unknown_faces_screen.dart';
import '../../config.dart';

class AttendanceResultScreen extends StatefulWidget {
  static final routeName = '/attendance-result-screen';
  static final title = 'Attendance Data';

  @override
  _AttendanceResultScreenState createState() => _AttendanceResultScreenState();
}

class _AttendanceResultScreenState extends State<AttendanceResultScreen> {
  List<dynamic> absentList = [];
  List<dynamic> presentList = [];
  List<dynamic> absentNames = [];
  List<dynamic> presentNames = [];
  String slot_id = '';
  int presentCount = 0;
  int absentCount = 0;
  String token;

  @override
  void didChangeDependencies() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
    });
    print(ModalRoute.of(context).settings.arguments);
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    print("route args");
    print(routeArgs);
    setState(() {
      absentList = routeArgs['absentList'];
      absentNames = routeArgs['absentNames'];
      presentNames = routeArgs['presentNames'];
      presentList = routeArgs['presentList'];
      presentCount = routeArgs['presentCount'];
      absentCount = routeArgs['absentCount'];
      slot_id = routeArgs['slot_id'];
    });
    super.didChangeDependencies();
  }

  void discardAttendance() async {
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    var url = "$baseUrl/deleteattendance/$slot_id";
    var r = await delete(Uri.encodeFull(url), headers: headers);
    Toast.show("Attendance Discarded", context, gravity: Toast.CENTER);
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AttendanceResultScreen.title),
      ),
      body: Container(
        padding: EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Card(
              child: Container(
                width: double.infinity,
                padding: const EdgeInsets.all(8.0),
                child: Text("Total Present : $presentCount"),
              ),
            ),
            Card(
              child: Container(
                width: double.infinity,
                padding: const EdgeInsets.all(8.0),
                child: Text("Total Absent : $absentCount"),
              ),
            ),
            Card(
              child: Container(
                width: double.infinity,
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context)
                        .pushNamed(AttendanceListScreen.routeName, arguments: {
                      'title': 'Absentee List',
                      'data': absentList,
                      'names': absentNames,
                    });
                  },
                  child: Text('View Absentee List'),
                ),
              ),
            ),
            Card(
              child: Container(
                width: double.infinity,
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context)
                        .pushNamed(AttendanceListScreen.routeName, arguments: {
                      'title': 'Present List',
                      'data': presentList,
                      'names': presentNames,
                    });
                  },
                  child: Text('View Present List'),
                ),
              ),
            ),
            Card(
              child: Container(
                width: double.infinity,
                padding: EdgeInsets.all(8),
                child: GestureDetector(
                  onTap: () => Navigator.of(context)
                      .pushNamed(UnknownFaces.routeName, arguments: slot_id),
                  child: Text('View Unknown Faces'),
                ),
              ),
            ),
            Card(
              child: Container(
                width: double.infinity,
                padding: EdgeInsets.all(8),
                child: GestureDetector(
                  onTap: () => Navigator.of(context)
                      .pushNamed(KnownFaces.routeName, arguments: slot_id),
                  child: Text('View Known Faces'),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: discardAttendance,
        backgroundColor: Colors.red,
        icon: Icon(Icons.delete),
        label: Text('Discard'),
      ),
    );
  }
}
