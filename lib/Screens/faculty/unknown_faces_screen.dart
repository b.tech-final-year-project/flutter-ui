import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';

import '../../config.dart';

class UnknownFaces extends StatefulWidget {
  static final routeName = '/unknown-faces';
  @override
  _UnknownFacesState createState() => _UnknownFacesState();
}

class _UnknownFacesState extends State<UnknownFaces> {
  bool _isLoadingInitData = true;
  String slotId, token;
  dynamic unknownFaces = [];

  void loadInitData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('token');
    setState(() {
      token = stringValue;
    });
    var url = '$baseUrl/detectedfaces/$slotId';
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    var r = await get(Uri.encodeFull(url), headers: headers);
    if (r.statusCode == 200) {
      setState(() {
        unknownFaces = jsonDecode(r.body)["unknown_file_urls"];
      });
    }

    setState(() {
      _isLoadingInitData = false;
    });
  }

  @override
  void didChangeDependencies() {
    if (_isLoadingInitData) {
      setState(() {
        slotId = ModalRoute.of(context).settings.arguments;
      });

      loadInitData();
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Unknown faces'),
      ),
      body: _isLoadingInitData
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              itemCount: unknownFaces.length,
              // children: List.generate(unknownFaces.length, (index) {
              itemBuilder: (context, index) {
                String imageName = unknownFaces[index].toString();
                var pos = imageName.lastIndexOf('-');
                print(imageName);
                String score = (pos != -1)
                    ? imageName.substring(pos + 1, pos + 5)
                    : imageName;
                var posName = imageName.lastIndexOf('_');
                String studentName = (posName != -1)
                    ? imageName.substring(posName + 1, pos)
                    : imageName;
                return ListTile(
                  leading: Image.network(
                    "$baseUrl${unknownFaces[index]}",
                  ),
                  title: Text(
                    Uri.decodeFull(studentName),
                  ),
                  subtitle: Text("Probability" + score),
                );
              },
            ),
    );
  }
}
