import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';
import 'package:toast/toast.dart';

import '../../config.dart';
import 'enrolled_students_screen.dart';
import 'enroll_students_screen.dart';
import 'view_previous_attendance_screen.dart';
import 'low_attendance_screen.dart';

class ManageCourseScreen extends StatefulWidget {
  static final routeName = '/manage-course-screen';
  @override
  _ManageCourseScreenState createState() => _ManageCourseScreenState();
}

class _ManageCourseScreenState extends State<ManageCourseScreen> {
  var _numberOfEnrolledStudents = 0;
  bool _isRaisingTicket = false;
  var _loadedInitData = false;
  bool _isTraing = false;
  dynamic enrolledStudents;
  dynamic currentCourse;
  String token = '';
  int total_classes = 0;
  dynamic goodAttendance = [];
  dynamic lowAttendance = [];
  var _issueInputController = TextEditingController();

  @override
  void didChangeDependencies() {
    if (_loadedInitData == false) {
      final routeArgs = ModalRoute.of(context).settings.arguments;
      setState(() {
        _loadedInitData = true;
        currentCourse = routeArgs;
      });
      _loadEnrolledStudents();
    }
    super.didChangeDependencies();
  }

  Future<String> _loadEnrolledStudents() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('token');
    setState(() {
      token = stringValue;
    });
    var response = await http.get(
      Uri.encodeFull(baseUrl + '/enroll_students/${currentCourse.id}'),
      headers: {
        'x-access-token': token,
        'Accept': 'Application/json',
      },
    );
    var data = jsonDecode(response.body);
    data = data['students'];
    print(data);

    var url = "$baseUrl/attendance/statistics/${currentCourse.id}";
    var r = await http.get(Uri.encodeFull(url), headers: {
      'x-access-token': token,
      'Accept': 'Application/json',
    });
    var jsonData = jsonDecode(r.body);

    setState(() {
      goodAttendance = jsonData["80%"];
      lowAttendance = jsonData["<80%"];
      total_classes = jsonData["Total number of classes"];
      enrolledStudents = data;
      _numberOfEnrolledStudents =
          enrolledStudents == null ? 0 : enrolledStudents.length;
    });
  }

  void raiseTicket() async {
    setState(() {
      _isRaisingTicket = true;
    });

    Uri url = Uri.parse("$baseUrl/faculty/ticket");

    MultipartRequest request = MultipartRequest("POST", url);
    request.fields["id"] = currentCourse.id.toString();
    request.fields["issue"] = _issueInputController.text;
    request.headers["x-access-token"] = token;

    try {
      final streamedResponse = await request.send();
      final response = await Response.fromStream(streamedResponse);
      if (response.statusCode == 200) {
        print("Success");
        var jsonData = jsonDecode(response.body);
        if (jsonData["Status"] == "Success") {
          Toast.show(jsonData["message"], context,
              duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
          setState(() {
            _isRaisingTicket = false;
          });
        }
      } else {
        print("Error");
      }
    } on Exception catch (_) {}
  }

  void _raiseTicket(BuildContext ctx) {
    showModalBottomSheet(
      context: ctx,
      builder: (_) {
        return GestureDetector(
          onTap: () {},
          child: Container(
            padding: EdgeInsets.all(16),
            child: Column(
              children: <Widget>[
                TextFormField(
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  maxLength: 1000,
                  controller: _issueInputController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Issue',
                      labelStyle: TextStyle(fontSize: 15)),
                ),
                _isRaisingTicket
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : FlatButton(
                        height: 50,
                        color: Theme.of(context).primaryColor,
                        textColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        onPressed: () {
                          raiseTicket();
                          Navigator.of(context).pop();
                          _issueInputController.clear();
                        },
                        child: Text('Raise Ticket'),
                      )
              ],
            ),
          ),
          behavior: HitTestBehavior.opaque,
        );
      },
    );
  }

  Future<void> _reTrain() async {
    Toast.show("Started Training", context, gravity: Toast.CENTER);
    setState(() {
      _isTraing = true;
    });
    var url = '$baseUrl/train_model/${currentCourse.id}';
    String _body = '{"retrain" : 1}';
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };

    var r = await post(Uri.encodeFull(url), headers: headers, body: _body);

    if (r.statusCode != 200) {
      Toast.show(jsonDecode(r.body)["message"], context);
    } else {
      Toast.show("Training completed", context, gravity: Toast.CENTER);
      setState(() {
        _isTraing = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(currentCourse.name),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(8),
            child: Column(
              children: <Widget>[
                Card(
                  elevation: 1,
                  child: Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: <Widget>[
                        Text(currentCourse.code),
                        SizedBox(
                          height: 10,
                        ),
                        Text(currentCourse.name),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RaisedButton(
                    elevation: 5,
                    color: Theme.of(context).accentColor,
                    onPressed: () {
                      setState(() {
                        _loadedInitData = false;
                      });
                      Navigator.of(context).pushNamed(
                        EnrolledStudetsScreen.routeName,
                        arguments: {
                          'currentCourse': currentCourse,
                        },
                      ).then((_) {
                        _loadEnrolledStudents();
                      });
                    },
                    child: Container(
                      width: double.infinity,
                      padding: EdgeInsets.all(10),
                      child: Center(
                        child: Text(
                          'View Enrolled Students ( $_numberOfEnrolledStudents )',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RaisedButton(
                    elevation: 5,
                    color: Theme.of(context).accentColor,
                    onPressed: () {
                      Navigator.of(context).pushNamed(
                        EnrollStudentsScreen.routeName,
                        arguments: {
                          'currentCourse': currentCourse,
                          'enrolledStudents': enrolledStudents,
                        },
                      );
                    },
                    child: Container(
                      width: double.infinity,
                      padding: EdgeInsets.all(10),
                      child: Center(
                        child: Text(
                          'Enroll Students',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  child: Card(
                      elevation: 1,
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          children: <Widget>[
                            Text(
                              "Attendance Statistics",
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                            Divider(),
                            Text(
                                "Total classes : ${total_classes == null ? 0 : total_classes}"),
                            Text(
                                "Students with attendance at least 80% : ${goodAttendance == null ? 0 : goodAttendance.length}"),
                            Text(
                                "Students with less than 80% : ${lowAttendance == null ? 0 : lowAttendance.length}"),
                            Divider(),
                            RaisedButton(
                              elevation: 5,
                              color: Theme.of(context).accentColor,
                              onPressed: () => Navigator.of(context).pushNamed(
                                LowAttendanceScreen.routeName,
                                arguments: lowAttendance,
                              ),
                              child: Container(
                                width: double.infinity,
                                padding: EdgeInsets.all(10),
                                child: Center(
                                  child: Text(
                                    'View Students with low attendance',
                                    style: TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )),
                ),
                // Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: RaisedButton(
                //     elevation: 5,
                //     color: Theme.of(context).accentColor,
                //     onPressed: () {
                //       Navigator.of(context).pushNamed(
                //         PreviousAttendance.routeName,
                //         arguments: {
                //           'currentCourse': currentCourse,
                //         },
                //       );
                //     },
                //     child: Container(
                //       width: double.infinity,
                //       padding: EdgeInsets.all(10),
                //       child: Center(
                //         child: Text(
                //           'View Previous Attendance',
                //           style: TextStyle(
                //             color: Colors.white,
                //           ),
                //         ),
                //       ),
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
        ),
        // floatingActionButton: FloatingActionButton.extended(
        //   onPressed: () => _raiseTicket(context),
        //   icon: Icon(Icons.assistant_photo),
        //   label: Text('Raise Ticket'),
        // ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FloatingActionButton.extended(
                onPressed: () => _reTrain(),
                heroTag: "btn2",
                icon: Icon(Icons.settings),
                label: Text('Retrain Model'),
              ),
              FloatingActionButton.extended(
                onPressed: () => _raiseTicket(context),
                heroTag: "btn1",
                icon: Icon(Icons.assistant_photo),
                label: Text('Raise Ticket'),
              ),
            ],
          ),
        ));
  }
}
