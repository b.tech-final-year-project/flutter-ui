import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';

import '../../config.dart';

class KnownFaces extends StatefulWidget {
  static final routeName = '/known-faces';
  @override
  _KnownFacesState createState() => _KnownFacesState();
}

class _KnownFacesState extends State<KnownFaces> {
  bool _isLoadingInitData = true;
  String slotId, token;
  dynamic knownFaces = [];

  void loadInitData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('token');
    setState(() {
      token = stringValue;
    });
    var url = '$baseUrl/detectedfaces/$slotId';
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    var r = await get(Uri.encodeFull(url), headers: headers);
    if (r.statusCode == 200) {
      setState(() {
        knownFaces = jsonDecode(r.body)["known_file_urls"];
      });
    }

    setState(() {
      _isLoadingInitData = false;
    });
  }

  @override
  void didChangeDependencies() {
    if (_isLoadingInitData) {
      setState(() {
        slotId = ModalRoute.of(context).settings.arguments;
      });

      loadInitData();
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Known faces'),
      ),
      body: _isLoadingInitData
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              itemCount: knownFaces.length,
              // children: List.generate(unknownFaces.length, (index) {
              itemBuilder: (context, index) {
                String imageName = knownFaces[index].toString();
                var pos = imageName.lastIndexOf('-');
                print(imageName);
                String score = (pos != -1)
                    ? imageName.substring(pos + 1, pos + 5)
                    : imageName;
                var posName = imageName.lastIndexOf('_');
                String studentName = (posName != -1)
                    ? imageName.substring(posName + 1, pos)
                    : imageName;
                return ListTile(
                  leading: Image.network(
                    "$baseUrl${knownFaces[index]}",
                  ),
                  title: Text(
                    Uri.decodeFull(studentName),
                  ),
                  subtitle: Text("Probability " + score),
                );
              },
            ),
    );
  }
}
