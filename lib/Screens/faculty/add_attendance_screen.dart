import 'dart:io';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime/mime.dart';
import 'package:toast/toast.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Models/Course.dart';
import '../../config.dart';
import 'attendance_result_screen.dart';

class AddAttendanceScreen extends StatefulWidget {
  static final routeName = '/add-attendance-multi';
  static final title = 'Add Attendance';

  @override
  _AddAttendanceScreenState createState() => _AddAttendanceScreenState();
}

class _AddAttendanceScreenState extends State<AddAttendanceScreen> {
  List<Course> courseList = [];
  File imageFile;
  String token = '';
  final url = "$baseUrl/attendance";
  bool _isUploading = false;
  Course selectedCourse;
  List<DropdownMenuItem<Course>> _dropdownMenuItems;

  Future<String> _loadCourseList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('token');
    setState(() {
      token = stringValue;
    });
    var response = await http.get(
      Uri.encodeFull(baseUrl + '/courses'),
      headers: {
        'x-access-token': token,
        'Accept': 'Application/json',
      },
    );
    var data = jsonDecode(response.body);
    var courses = data['courses'];
    for (int i = 0; i < courses.length; i++) {
      var course = courses[i];
      var newCourse = new Course(
        id: course['id'],
        code: course['code'],
        name: course['name'],
        instructorId: course['instructor_id'],
        academicSession: course['academic_session'],
      );
      setState(() {
        courseList.add(newCourse);
        _dropdownMenuItems = buildDropdownMenuItems(courseList);
        selectedCourse = _dropdownMenuItems[0].value;
      });
    }
  }

  List<DropdownMenuItem<Course>> buildDropdownMenuItems(List courses) {
    List<DropdownMenuItem<Course>> items = List();
    for (Course course in courses) {
      items.add(
        DropdownMenuItem(
          value: course,
          child: Text(course.name),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(Course courseSelected) {
    print("${courseSelected.name} Selected");
    setState(() {
      selectedCourse = courseSelected;
    });
  }

  _openGallery(BuildContext ctx) async {
    File pic = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      imageFile = pic;
    });
    Navigator.of(ctx).pop();
  }

  _openCamera(BuildContext ctx) async {
    File pic = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      imageFile = pic;
    });
    Navigator.of(ctx).pop();
  }

  Future<void> _showChoiceDialouge(BuildContext ctx) {
    return showDialog(
        context: ctx,
        builder: (BuildContext ctx) {
          return AlertDialog(
            title: Text('Make a choice'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GestureDetector(
                      child: Text('Gallery'),
                      onTap: () {
                        _openGallery(ctx);
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GestureDetector(
                      child: Text('Camera'),
                      onTap: () {
                        _openCamera(ctx);
                      },
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget _displayImage() {
    if (imageFile == null) {
      return Text('No image selected');
    } else {
      return Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Image.file(
                imageFile,
                width: 50,
                height: 50,
              ),
              Text('Image selected')
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: OutlineButton(
              onPressed: _isUploading ? null : _startUploading,
              child: _isUploading
                  ? Text('Processing')
                  : Text('Register Attendance'),
            ),
          ),
        ],
      );
    }
  }

  Future<Map<String, dynamic>> _uploadImage(File image, String token) async {
    setState(() {
      _isUploading = true;
    });
    final mimeTypeData =
        lookupMimeType(image.path, headerBytes: [0xFF, 0xD8]).split('/');
    final imageUploadRequest = http.MultipartRequest('POST', Uri.parse(url));
    final file = await http.MultipartFile.fromPath('file', image.path,
        contentType: MediaType(mimeTypeData[0], mimeTypeData[1]));
    imageUploadRequest.fields['ext'] = mimeTypeData[1];
    imageUploadRequest.fields['id'] = '1';
    imageUploadRequest.files.add(file);
    imageUploadRequest.headers["x-access-token"] = token;
    try {
      final streamedResponse = await imageUploadRequest.send();
      final response = await http.Response.fromStream(streamedResponse);
      if (response.statusCode != 200) {
        return null;
      }
      final Map<String, dynamic> responseData = json.decode(response.body);
      _resetState();
      return responseData;
    } catch (e) {
      print(e);
      return null;
    }
  }

  void _startUploading() async {
    final Map<String, dynamic> response = await _uploadImage(imageFile, token);
    final absentList = response['absent_list'];
    final absentCount = response['absentCount'];
    final presentList = response['present_list'];
    final presentCount = response['presentCount'];
    final absentNames = response['absent_names'];
    final presentNames = response['present_names'];
    print("absent names : " + absentList);
    print(presentList);

    Navigator.of(context)
        .pushNamed(AttendanceResultScreen.routeName, arguments: {
      'absentList': absentList,
      'absentCount': absentCount,
      'presentList': presentList,
      'presentCount': presentCount,
      'presentNames': presentNames,
      'absentNames': absentNames,
      // 'slot_id': response['slot_id'],
    });

    // Check if any error occured
    if (response == null || response.containsKey("error")) {
      Toast.show("Image Upload Failed!!!", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    } else {
      Toast.show("Image Uploaded Successfully!!!", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }
  }

  void _resetState() {
    setState(() {
      _isUploading = false;
      imageFile = null;
    });
  }

  @override
  void initState() {
    _loadCourseList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AddAttendanceScreen.title),
      ),
      // drawer: MainDrawer('Test'),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 20,
                ),
                Text('Select Course'),
                SizedBox(
                  height: 10,
                ),
                DropdownButton(
                  value: selectedCourse,
                  items: _dropdownMenuItems,
                  onChanged: onChangeDropdownItem,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Course Code',
                    prefixIcon: Icon(Icons.lock_outline),
                    labelStyle: TextStyle(fontSize: 15),
                  ),
                ),
                OutlineButton(
                  onPressed: () {
                    _showChoiceDialouge(context);
                  },
                  child: Text('Select image'),
                ),
                _displayImage()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
