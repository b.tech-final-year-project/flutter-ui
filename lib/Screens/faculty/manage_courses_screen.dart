import 'dart:convert';

import 'package:attendance_app/Widgets/course_item.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

import '../../Models/Course.dart';
import '../../config.dart';
import './manage_course_screen.dart';

class ManageCoursesScreen extends StatefulWidget {
  static final routeName = '/manage-course';
  static final title = 'Manage Courses';
  @override
  _ManageCoursesScreenState createState() => _ManageCoursesScreenState();
}

class _ManageCoursesScreenState extends State<ManageCoursesScreen> {
  List<Course> courseList = [];
  String token = '';
  final _thresholdInputController = TextEditingController();
  dynamic courses = [];

  Future<String> loadCourseList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('token');
    setState(() {
      token = stringValue;
    });
    var response = await http.get(
      Uri.encodeFull(baseUrl + '/courses'),
      headers: {
        'x-access-token': token,
        'Accept': 'Application/json',
      },
    );
    var jsonData = jsonDecode(response.body);
    courses = jsonData['courses'];
    if (courses != null) {
      for (int i = 0; i < courses.length; i++) {
        var course = courses[i];
        print("threasholdd11");
        print(course);
        var newCourse = new Course(
          id: course['id'],
          code: course['code'],
          name: course['name'],
          instructorId: course['instructor_id'],
          academicSession: course['academic_session'],
          thresholdValue: course['threshold'].toString(),
        );
        setState(() {
          courseList.add(newCourse);
        });
      }
    }
  }

  @override
  void initState() {
    loadCourseList();
    super.initState();
  }

  void deleteCourse(int id) async {
    var delUrl = "$baseUrl/courses/$id";
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    var r = await http.delete(Uri.encodeFull(delUrl), headers: headers);
  }

  Future<bool> changeCoursethreshold(int id) async {
    String thresholdvalue = _thresholdInputController.text.trim();
    var changeUrl = "$baseUrl/courses/$id";
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    String _body = '{"threshold":"$thresholdvalue"}';
    var r = await http.put(Uri.encodeFull(changeUrl),
        headers: headers, body: _body);
    if (r.statusCode == 200) {
      Toast.show("Successfully Updated Threshold", context,
          gravity: Toast.CENTER);
      _thresholdInputController.clear();
      return true;
    } else {
      Toast.show("Threshold Update Failed", context, gravity: Toast.CENTER);
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(ManageCoursesScreen.title),
      ),
      body: courseList.length == 0
          ? Center(
              child: Text(
                'You\'re not offering any courses',
                style: TextStyle(fontSize: 16, color: Colors.grey),
              ),
            )
          : ListView.builder(
              itemBuilder: (ctx, index) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 8),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).pushNamed(
                              ManageCourseScreen.routeName,
                              arguments: courseList[index]);
                        },
                        child: Card(
                          elevation: 3,
                          child: Container(
                            width: double.infinity,
                            padding: const EdgeInsets.all(20.0),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: CourseItem(
                                    token,
                                    courses[index],
                                  ),
                                ),
                                IconButton(
                                  icon: Icon(
                                    Icons.edit,
                                    color: Colors.blueAccent,
                                  ),
                                  onPressed: () {
                                    AlertDialog alert = AlertDialog(
                                      title: Text(
                                          "Current Value : ${courseList[index].thresholdValue}"),
                                      content: TextField(
                                        controller: _thresholdInputController,
                                        onChanged: (value) {},
                                        decoration: InputDecoration(
                                          labelText: 'New threshold',
                                        ),
                                      ),
                                      actions: [
                                        RaisedButton(
                                          color: Colors.green,
                                          child: Text("Confirm"),
                                          onPressed: () {
                                            if (changeCoursethreshold(
                                                    courseList[index].id) !=
                                                null) {
                                              final courseVal = courseList
                                                  .firstWhere((element) =>
                                                      element.id ==
                                                      courseList[index].id);
                                              setState(() {
                                                courseVal.thresholdValue =
                                                    _thresholdInputController
                                                        .text
                                                        .trim();
                                              });
                                            }
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                        RaisedButton(
                                          color: Colors.red,
                                          child: Text("Cancel"),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        )
                                      ],
                                    );
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return alert;
                                      },
                                    );
                                  },
                                ),
                                IconButton(
                                  icon: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                  onPressed: () {
                                    AlertDialog alert = AlertDialog(
                                      title: Text("Confirm delete"),
                                      content: Text(
                                          "You're deleting : ${courseList[index].name}"),
                                      actions: [
                                        RaisedButton(
                                          color: Colors.green,
                                          child: Text("Confirm"),
                                          onPressed: () {
                                            deleteCourse(courseList[index].id);
                                            setState(() {
                                              courseList.removeWhere(
                                                  (element) =>
                                                      element.id ==
                                                      courseList[index].id);
                                            });
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                        RaisedButton(
                                          color: Colors.red,
                                          child: Text("Cancel"),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        )
                                      ],
                                    );
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return alert;
                                      },
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              },
              itemCount: courseList.length,
            ),
    );
  }
}
