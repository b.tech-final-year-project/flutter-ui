import 'package:flutter/material.dart';
import 'package:flutter/material.dart';

import '../../config.dart';

class PreviousImages extends StatefulWidget {
  @override
  _PreviousImages createState() => _PreviousImages();
}

class _PreviousImages extends State<PreviousImages> {
  @override
  Widget build(BuildContext context) {
    dynamic preImages = [];
    preImages = ModalRoute.of(context).settings.arguments;
    return new Scaffold(
      appBar: AppBar(
        title: Text('Previous Images'),
      ),
      body: preImages == null
          ? Center(
              child: Text(
                'You\'ve not uploaded any images for this student yet',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              ),
            )
          : GridView.count(
              shrinkWrap: true,
              crossAxisCount: 3,
              children: List.generate(preImages.length, (index) {
                // Asset asset = preImages[index];
                return Image.network(
                  "$baseUrl${preImages[index]}",
                  width: 300,
                  height: 300,
                );
              }),
            ),
    );
  }
}
