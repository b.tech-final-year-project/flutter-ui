import 'dart:async';
import 'dart:convert';
import 'dart:ffi';
import 'package:attendance_app/Auth/google_sign_in.dart';
import 'package:attendance_app/Screens/admin/add_admin_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../Widgets/main_drawer.dart';
import '../../Widgets/main_screen_link.dart';
// import 'add_attendance_screen.dart';
import '../../config.dart';
import '../admin/add_faculty_screen.dart';
import '../admin/manage_faculties_screen.dart';
import '../admin/add_student_screen.dart';
import '../admin/manage_students_screen.dart';
import '../admin/view_tickets.dart';
import '../settings_screen.dart';
import 'manage_admins.dart';

class AdminDashboard extends StatefulWidget {
  static final routeName = '/AdminDashboard';
  @override
  _AdminDashboardState createState() => _AdminDashboardState();
}

class _AdminDashboardState extends State<AdminDashboard> {
  String token = '';
  String currentUser = '';
  var links = [];

  void logout() async {
    final prefrences = await SharedPreferences.getInstance();
    await prefrences.clear();
    signOutGoogle();
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  Future<Void> getCurrentUserData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('token');
    bool admin = prefs.getBool('admin');
    if (admin) {
      setState(() {
        links = [
          {
            'title': 'Add Faculty',
            'icon': Icons.add_circle_outline,
            'route': AddFaculty.routeName,
          },
          {
            'title': 'Manage Faculties',
            'icon': Icons.group,
            'route': ManageFaculties.routeName,
          },
          {
            'title': 'Add Student ',
            'icon': Icons.add_circle_outline,
            'route': AddStudent.routeName,
          },
          {
            'title': 'Manage Students',
            'icon': Icons.group,
            'route': ManageStudents.routeName,
          },
          {
            'title': 'Add Admins',
            'icon': Icons.add_circle_outline,
            'route': AddAdmin.routeName,
          },
          {
            'title': 'Manage Admins',
            'icon': Icons.group,
            'route': ManageAdmins.routeName,
          },
          {
            'title': 'View Tickets',
            'icon': Icons.assistant_photo,
            'route': ViewTickets.routeName,
          },
        ];
      });
    }
    setState(() {
      token = stringValue;
    });
    var response = await http.get(
      Uri.encodeFull(baseUrl + '/login/admin'),
      headers: {
        'x-access-token': token,
        'Accept': 'Application/json',
      },
    );
    var data = jsonDecode(response.body);
    setState(() {
      currentUser = data['data']['name'];
    });
  }

  @override
  void initState() {
    getCurrentUserData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Attendance App'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.settings),
              onPressed: () =>
                  Navigator.of(context).pushNamed(SettingsScreen.routeName)),
          IconButton(icon: Icon(Icons.power_settings_new), onPressed: logout)
        ],
      ),
      drawer: MainDrawer(currentUser),
      body: Container(
        margin: EdgeInsets.only(top: 12),
        child: ListView.builder(
          itemBuilder: (ctx, index) {
            return MainScreenLink(links[index]['title'], links[index]['route'],
                links[index]['icon']);
          },
          itemCount: links.length,
        ),
      ),
    );
  }
}
