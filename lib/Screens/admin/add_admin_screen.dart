import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:toast/toast.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../config.dart';
import 'manage_admins.dart';

class AddAdmin extends StatefulWidget {
  static final routeName = '/add-admin';
  @override
  _AddAdminState createState() => _AddAdminState();
}

class _AddAdminState extends State<AddAdmin> {
  bool _isLoadingInitData = true;
  String token;
  bool _isCreatingFaculty = false;
  var _passwordInputController = TextEditingController();
  var _nameInputController = TextEditingController();
  var _emailInputController = TextEditingController();

  void loadInitData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('token');
    setState(() {
      token = stringValue;
    });
    setState(() {
      _isLoadingInitData = false;
    });
  }

  @override
  void initState() {
    loadInitData();
    super.initState();
  }

  void validateAddFaculty() async {
    setState(() {
      _isCreatingFaculty = true;
    });
    String name = _nameInputController.text.trim();
    String email = _emailInputController.text.trim();
    String password = _passwordInputController.text.trim();

    String url = "$baseUrl/admin";
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    String _body =
        '{"email_id":"$email","name":"$name","password":"$password"}';
    var r = await post(Uri.encodeFull(url), headers: headers, body: _body);
    var jsonData = jsonDecode(r.body);
    Toast.show(jsonData["message"], context,
        duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
    if (jsonData["status"] == "Success") {
      Navigator.of(context).pushReplacementNamed(ManageAdmins.routeName);
    }
    setState(() {
      _isCreatingFaculty = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Admin'),
      ),
      body: _isLoadingInitData
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: EdgeInsets.all(16),
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: TextFormField(
                      controller: _nameInputController,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Name',
                          prefixIcon: Icon(Icons.person),
                          labelStyle: TextStyle(fontSize: 15)),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: TextFormField(
                      controller: _emailInputController,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Email',
                          prefixIcon: Icon(Icons.email),
                          labelStyle: TextStyle(fontSize: 15)),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: TextFormField(
                      controller: _passwordInputController,
                      obscureText: true,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Password',
                          prefixIcon: Icon(Icons.lock_outline),
                          labelStyle: TextStyle(fontSize: 15)),
                    ),
                  ),
                  _isCreatingFaculty
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : FlatButton(
                          height: 50,
                          color: Colors.green[300],
                          textColor: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          onPressed: validateAddFaculty,
                          child: Text('Add Admin'),
                        )
                ],
              ),
            ),
    );
  }
}
