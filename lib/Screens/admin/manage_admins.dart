import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';
import 'package:toast/toast.dart';

import '../../config.dart';
import '../../Widgets/admin_item.dart';

class ManageAdmins extends StatefulWidget {
  static final routeName = '/manage-admins';
  @override
  _ManageAdminsState createState() => _ManageAdminsState();
}

class _ManageAdminsState extends State<ManageAdmins> {
  String token;
  bool _isLoadingInitData = true;
  dynamic adminList = [];

  Future<String> deleteAdmin(int id) async {
    String idstring = id.toString();
    String delUrl = "$baseUrl/admin/$idstring";
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    var r = await delete(Uri.encodeFull(delUrl), headers: headers);
    if (r.statusCode == 200) {
      var jsonData = jsonDecode(r.body);
      Toast.show(jsonData["message"], context, duration: Toast.LENGTH_LONG);
      if (jsonData["status"] == "Success") {
        setState(() {
          adminList.removeWhere((e) => e["id"] == id);
        });
      }
    }
    return 'Deleted';
  }

  void loadInitData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('token');
    setState(() {
      token = stringValue;
    });
    String url = "$baseUrl/admins";
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    var r = await get(Uri.encodeFull(url), headers: headers);
    if (r.statusCode == 200) {
      setState(() {
        adminList = jsonDecode(r.body)["admins"];
      });
    } else {
      print(r.statusCode);
      print(r.body);
    }
    setState(() {
      _isLoadingInitData = false;
    });
  }

  @override
  void initState() {
    loadInitData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Manage Admins'),
      ),
      body: _isLoadingInitData
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: EdgeInsets.all(8),
              child: adminList == null
                  ? Center(
                      child: Text(
                        'No active Admins',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.grey,
                        ),
                      ),
                    )
                  : ListView.builder(
                      itemBuilder: (ctx, index) {
                        return AdminItem(token, adminList[index], deleteAdmin);
                        // return Text(adminList[index]["name"]);
                      },
                      itemCount: adminList == null ? 0 : adminList.length,
                    ),
            ),
    );
  }
}
