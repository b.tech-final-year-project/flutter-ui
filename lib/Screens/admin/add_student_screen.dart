import 'dart:convert';
import 'dart:io';
import 'package:path/path.dart';

import 'package:http_parser/http_parser.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mime/mime.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:file_picker/file_picker.dart';

import '../../config.dart';
import 'manage_students_screen.dart';

class AddStudent extends StatefulWidget {
  static final routeName = '/add-student';
  @override
  _AddStudentState createState() => _AddStudentState();
}

class _AddStudentState extends State<AddStudent> {
  String token;
  File csvFile;
  Uri url = Uri.parse("$baseUrl/students");
  bool _isLoadingInitData = true;
  bool _isAddingStudent = false;
  var _nameInputController = TextEditingController();
  var _regnoInputController = TextEditingController();
  var _emailInputController = TextEditingController();
  var _passwordInputController = TextEditingController();

  void loadInitData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
    });
    setState(() {
      _isLoadingInitData = false;
    });
  }

  @override
  void initState() {
    loadInitData();
    super.initState();
  }

  void validateAddStudent(BuildContext context) async {
    String regno = _regnoInputController.text.trim();
    String name = _nameInputController.text.trim();
    String email = _emailInputController.text.trim();
    String password = _passwordInputController.text.trim();
    setState(() {
      _isAddingStudent = true;
    });
    MultipartRequest request = MultipartRequest("POST", url);
    request.fields["regno"] = regno;
    request.fields["name"] = name;
    request.fields["email_id"] = email;
    request.fields["password"] = password;
    request.headers["x-access-token"] = token;

    try {
      final streamedResponse = await request.send();
      final response = await Response.fromStream(streamedResponse);
      print(response.statusCode);
      if (response.statusCode == 200) {
        print("Success");
        var jsonData = jsonDecode(response.body);
        if (jsonData["status"] == "Success") {
          Toast.show(jsonData["status"], context,
              duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
          Navigator.of(context).pushReplacementNamed(ManageStudents.routeName);
        }
      } else {
        print("Error");
      }
    } on Exception catch (_) {}
    setState(() {
      _isAddingStudent = false;
    });
  }

  Future<Map<String, dynamic>> _uploadImage(File image, String token) async {
    setState(() {
      _isAddingStudent = true;
    });
    var url = "$baseUrl/studentscsv";
    final mimeTypeData =
        lookupMimeType(image.path, headerBytes: [0xFF, 0xD8]).split('/');
    final imageUploadRequest = MultipartRequest('POST', Uri.parse(url));
    final file = await MultipartFile.fromPath('file', image.path,
        contentType: MediaType(mimeTypeData[0], mimeTypeData[1]));
    imageUploadRequest.fields['ext'] = mimeTypeData[1];
    imageUploadRequest.fields['id'] = '1';
    imageUploadRequest.files.add(file);
    imageUploadRequest.headers["x-access-token"] = token;
    try {
      final streamedResponse = await imageUploadRequest.send();
      final response = await Response.fromStream(streamedResponse);
      if (response.statusCode != 200) {
        return null;
      }
      final Map<String, dynamic> responseData = json.decode(response.body);

      return responseData;
    } catch (e) {
      print(e);
      return null;
    }
  }

  void _uploadCsvFile(BuildContext ctx) async {
    final Map<String, dynamic> response = await _uploadImage(csvFile, token);
    Toast.show(response["message"], ctx);
    Navigator.of(ctx).pushReplacementNamed(ManageStudents.routeName);
  }

  _openGallery(BuildContext ctx) async {
    // File pic = await ImagePicker.pickImage(source: ImageSource.gallery);
    File _file = await FilePicker.getFile(
      type: FileType.custom,
      allowedExtensions: ['csv'],
    );
    setState(() {
      csvFile = _file;
    });
    print(csvFile);
  }

  Widget _displayFile(BuildContext context) {
    if (csvFile == null) {
      return Text('No file selected');
    } else {
      return Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
                child: Text('File selected : ${basename(csvFile.path)}')),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _isAddingStudent
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : OutlineButton(
                    onPressed: () => _uploadCsvFile(context),
                    child: Text('Add Attendance'),
                  ),
          ),
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Student'),
      ),
      body: _isLoadingInitData
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: EdgeInsets.all(16),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: TextFormField(
                        controller: _nameInputController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Name',
                            prefixIcon: Icon(Icons.person),
                            labelStyle: TextStyle(fontSize: 15)),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: TextFormField(
                        controller: _regnoInputController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Roll number',
                            prefixIcon: Icon(Icons.person),
                            labelStyle: TextStyle(fontSize: 15)),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: TextFormField(
                        controller: _emailInputController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Email',
                            prefixIcon: Icon(Icons.person),
                            labelStyle: TextStyle(fontSize: 15)),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: TextFormField(
                        controller: _passwordInputController,
                        obscureText: true,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Password',
                            prefixIcon: Icon(Icons.person),
                            labelStyle: TextStyle(fontSize: 15)),
                      ),
                    ),
                    _isAddingStudent
                        ? CircularProgressIndicator()
                        : FlatButton(
                            height: 50,
                            color: Colors.green[300],
                            textColor: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            onPressed: () => validateAddStudent(context),
                            child: Text('Add Student'),
                          ),
                    SizedBox(
                      height: 24,
                    ),
                    Divider(),
                    SizedBox(
                      height: 24,
                    ),
                    Image.asset(
                      'assets/csv.png',
                      width: double.infinity,
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    OutlineButton(
                      onPressed: () {
                        _openGallery(context);
                      },
                      child: Text('Select CSV file'),
                    ),
                    _displayFile(context)
                  ],
                ),
              ),
            ),
    );
  }
}
