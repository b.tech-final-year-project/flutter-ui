import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';
import 'package:toast/toast.dart';

import '../../config.dart';
import '../../Widgets/faculty_item.dart';

class ManageFaculties extends StatefulWidget {
  static final routeName = '/manage-faculties';
  @override
  _ManageFacultiesState createState() => _ManageFacultiesState();
}

class _ManageFacultiesState extends State<ManageFaculties> {
  String token;
  bool _isLoadingInitData = true;
  dynamic facultyList = [];

  Future<String> deleteFaculty(String id) async {
    String delUrl = "$baseUrl/faculty/$id";
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    var r = await delete(Uri.encodeFull(delUrl), headers: headers);
    if (r.statusCode == 200) {
      var jsonData = jsonDecode(r.body);
      Toast.show(jsonData["message"], context, duration: Toast.LENGTH_LONG);
      if (jsonData["status"] == "Success") {
        setState(() {
          facultyList.removeWhere((e) => e["public_id"] == id);
        });
      }
    }
    return 'Deleted';
  }

  void loadInitData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('token');
    setState(() {
      token = stringValue;
    });
    String url = "$baseUrl/fac_all";
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    var r = await get(Uri.encodeFull(url), headers: headers);
    if (r.statusCode == 200) {
      setState(() {
        facultyList = jsonDecode(r.body)["faculties"];
      });
    } else {
      print(r.statusCode);
      print(r.body);
    }
    setState(() {
      _isLoadingInitData = false;
    });
  }

  @override
  void initState() {
    loadInitData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Manage Faculties'),
      ),
      body: _isLoadingInitData
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: EdgeInsets.all(8),
              child: facultyList == null
                  ? Center(
                      child: Text(
                        'No active faculties',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.grey,
                        ),
                      ),
                    )
                  : ListView.builder(
                      itemBuilder: (ctx, index) {
                        return FacultyItem(
                            token, facultyList[index], deleteFaculty);
                        // return Text(facultyList[index]["name"]);
                      },
                      itemCount: facultyList == null ? 0 : facultyList.length,
                    ),
            ),
    );
  }
}
