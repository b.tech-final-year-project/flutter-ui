import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Widgets/course_item.dart';
import '../../config.dart';

class ManageFaculty extends StatefulWidget {
  static final routeName = '/manage-faculty';
  @override
  _ManageFacultyState createState() => _ManageFacultyState();
}

class _ManageFacultyState extends State<ManageFaculty> {
  bool _isLoadingInitData = true;
  String token;
  dynamic faculty;
  dynamic courseList = [];

  @override
  void didChangeDependencies() {
    if (_isLoadingInitData) {
      setState(() {
        faculty = ModalRoute.of(context).settings.arguments;
      });
      print(faculty);
      loadInitData();
    }
    super.didChangeDependencies();
  }

  void loadInitData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
    });

    var url = "$baseUrl/requiredfacultycourses/${faculty["id"]}";

    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    var r = await get(Uri.encodeFull(url), headers: headers);

    if (r.statusCode == 200) {
      var jsonData = jsonDecode(r.body);
      setState(() {
        courseList = jsonData["courses"];
      });
    }

    print(courseList);

    setState(() {
      _isLoadingInitData = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return _isLoadingInitData
        ? Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          )
        : Scaffold(
            appBar: AppBar(
              title: Text(faculty["name"]),
            ),
            body: _isLoadingInitData
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Padding(
                    padding: EdgeInsets.all(8),
                    child: courseList == null
                        ? Center(
                            child: Text(
                              'Faculty is not offering any course',
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.grey,
                              ),
                            ),
                          )
                        : ListView.builder(
                            itemBuilder: (ctx, index) {
                              return Card(
                                elevation: 3,
                                child: Container(
                                  width: double.infinity,
                                  padding: const EdgeInsets.all(20.0),
                                  child: Row(
                                    children: [
                                      Expanded(
                                          child: CourseItem(
                                              token, courseList[index])),
                                    ],
                                  ),
                                ),
                              );
                            },
                            itemCount: courseList.length,
                          ),
                  ),
          );
  }
}
