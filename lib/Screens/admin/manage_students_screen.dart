import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';
import 'package:toast/toast.dart';

import '../../config.dart';
import '../../Widgets/student_item.dart';

class ManageStudents extends StatefulWidget {
  static final routeName = '/manage-students';
  @override
  _ManageStudentsState createState() => _ManageStudentsState();
}

class _ManageStudentsState extends State<ManageStudents> {
  String token;
  bool _isLoadingInitData = true;
  dynamic studentList = [];

  void deleteStudent(String regno) async {
    String delUrl = "$baseUrl/students/$regno";
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    var r = await delete(Uri.encodeFull(delUrl), headers: headers);
    if (r.statusCode == 200) {
      var jsonData = jsonDecode(r.body);
      Toast.show(jsonData["message"], context, duration: Toast.LENGTH_LONG);
      if (jsonData["status"] == "Success") {
        setState(() {
          studentList.removeWhere((e) => e["regno"] == regno);
        });
      }
    }
  }

  void loadInitData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('token');
    setState(() {
      token = stringValue;
    });
    String url = "$baseUrl/students";
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    var r = await get(Uri.encodeFull(url), headers: headers);
    if (r.statusCode == 200) {
      setState(() {
        studentList = jsonDecode(r.body)["students"];
      });
    } else {}
    setState(() {
      _isLoadingInitData = false;
    });
  }

  @override
  void initState() {
    loadInitData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Manage Students'),
      ),
      body: _isLoadingInitData
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: EdgeInsets.all(8),
              child: ListView.builder(
                itemBuilder: (ctx, index) {
                  return StudentItem(token, studentList[index], deleteStudent);
                  // return Text(studentList[index]["name"]);
                },
                itemCount: studentList.length,
              ),
            ),
    );
  }
}
