import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';
import 'package:toast/toast.dart';
import '../../Widgets/student_ticket_resolved.dart';
import '../../Widgets/student_ticket_unresolved.dart';
import '../../config.dart';

class StudentViewTickets extends StatefulWidget {
  static final routeName = '/view-student-tickets';
  @override
  _StudentViewTicketsState createState() => _StudentViewTicketsState();
}

class _StudentViewTicketsState extends State<StudentViewTickets> {
  bool _isloadinInitData = true;
  String token;
  dynamic ticketList = [];
  List statusList = new List();

  void loadInitData() async {
    var url = "$baseUrl/student/ticket";

    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
    });

    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    var r = await get(Uri.encodeFull(url), headers: headers);

    if (r.statusCode == 200) {
      var jsonData = jsonDecode(r.body);
      if (jsonData["status"] == "Success") {
        setState(() {
          ticketList = jsonData["tickets"];
        });
        for (int i = 0; i < ticketList.length; i++) {
          if (ticketList[i]["ticket_status"] == "pending")
            statusList.add(0);
          else
            statusList.add(1);
        }
        print(statusList);
      }
    } else {
      print(r.body);
    }

    print(ticketList);

    setState(() {
      _isloadinInitData = false;
    });
  }

  Future<void> updateStateValue(int value, int index, int id) async {
    var url = "$baseUrl/student/ticket/$id";
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };
    var map = jsonEncode({"status_type": 2});
    var r = await put(Uri.encodeFull(url), headers: headers, body: map);
    if (r.statusCode == 200) {
      var jsonData = jsonDecode(r.body);
      if (jsonData["status"] == "Success") {
        setState(() {
          statusList[index] = value;
        });
      }
      Toast.show(jsonData["message"], context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
    }
  }

  @override
  void initState() {
    loadInitData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(
                child: Text('Resolved Tickets'),
              ),
              Tab(
                child: Text('Unresolved Tickets'),
              ),
            ],
          ),
          title: Text('View Student Tickets'),
        ),
        body: _isloadinInitData
            ? Center(
                child: CircularProgressIndicator(),
              )
            : TabBarView(
                children: [
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: ListView.builder(
                      itemBuilder: (ctx, index) {
                        return TicketItem(token, ticketList[index],
                            statusList[index], index, updateStateValue);
                      },
                      itemCount: ticketList.length,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: ListView.builder(
                      itemBuilder: (ctx, index) {
                        return TicketItemU(token, ticketList[index],
                            statusList[index], index, updateStateValue);
                      },
                      itemCount: ticketList.length,
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
