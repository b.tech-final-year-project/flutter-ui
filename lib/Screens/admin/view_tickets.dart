import 'package:attendance_app/Widgets/main_screen_link.dart';
import 'package:flutter/material.dart';

import 'fac_view_tickets_screen.dart';
import 'student_view_tickets_screen.dart';

class ViewTickets extends StatefulWidget {
  static final routeName = '/admin-view-tickets';
  ViewTickets({Key key}) : super(key: key);

  _ViewTicketsState createState() => _ViewTicketsState();
}

class _ViewTicketsState extends State<ViewTickets> {
  final TextStyle dropdownMenuItem =
      TextStyle(color: Colors.black, fontSize: 18);

  final secondary = Color(0xfff29a94);

  final List<Map> user = [
    {
      "name": "Faculty Tickets",
      "icon": Icons.flag,
      'route': FacViewTickets.routeName,
    },
    {
      "name": "Student Tickets",
      "icon": Icons.flag,
      'route': StudentViewTickets.routeName,
    },
  ];

  @override
  Widget build(BuildContext context) {
    final primary = Theme.of(context).primaryColor;
    return Scaffold(
      appBar: AppBar(
        title: Text('Tickets'),
      ),
      body: Container(
        margin: EdgeInsets.only(top: 12),
        child: ListView.builder(
          itemBuilder: (ctx, index) {
            return MainScreenLink(
                user[index]['name'], user[index]['route'], user[index]['icon']);
          },
          itemCount: user.length,
        ),
      ),
    );
  }
}
