import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:toast/toast.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../config.dart';

class SettingsScreen extends StatefulWidget {
  static final routeName = '/settings-screen';
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool _isLoadingInitData = false;
  var _oldPasswordInputController = TextEditingController();
  var _newPasswordInputController = TextEditingController();
  String token;
  bool changingPassword = false;

  @override
  void didChangeDependencies() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      token = prefs.getString('token');
    });
    super.didChangeDependencies();
  }

  void changePassword() async {
    setState(() {
      changingPassword = true;
    });

    String oldPassword = _oldPasswordInputController.text;
    String newPassword = _newPasswordInputController.text;

    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-access-token": token
    };

    var url = "$baseUrl/changepassword";

    String _body = '{"password" : "$newPassword"}';

    var r = await post(Uri.encodeFull(url), headers: headers, body: _body);

    if (r.statusCode == 200) {
      Toast.show(jsonDecode(r.body)["message"], context, gravity: Toast.CENTER);
      Navigator.of(context).pop();
    } else {
      Toast.show("Error changing password", context, gravity: Toast.CENTER);
    }

    setState(() {
      changingPassword = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: Padding(
        padding: EdgeInsets.all(8),
        child: Card(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: TextFormField(
                  controller: _oldPasswordInputController,
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Old Password',
                      prefixIcon: Icon(Icons.lock_outline),
                      labelStyle: TextStyle(fontSize: 15)),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: TextFormField(
                  controller: _newPasswordInputController,
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'New Password',
                      prefixIcon: Icon(Icons.lock_outline),
                      labelStyle: TextStyle(fontSize: 15)),
                ),
              ),
              RaisedButton(
                onPressed: changePassword,
                child: Text('Change Password'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
