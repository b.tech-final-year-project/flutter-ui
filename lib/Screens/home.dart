import 'package:attendance_app/Screens/student/student_login_screen.dart';
import 'package:attendance_app/Widgets/list_item.dart';
import 'package:flutter/material.dart';

import 'admin/admin_login_screen.dart';
import 'faculty/login_screen.dart';

class Home extends StatefulWidget {
  static final routeName = '/';
  Home({Key key}) : super(key: key);

  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final TextStyle dropdownMenuItem =
      TextStyle(color: Colors.black, fontSize: 18);

  final secondary = Color(0xfff29a94);

  final List<Map> user = [
    {
      "name": "Admin",
      "icon": Icons.account_circle,
    },
    {
      "name": "Faculty",
      "icon": Icons.account_circle,
    },
    {
      "name": "Student",
      "icon": Icons.account_circle,
    },
  ];

  @override
  Widget build(BuildContext context) {
    final primary = Theme.of(context).primaryColor;
    return Scaffold(
      backgroundColor: Color(0xfff0f0f0),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 145),
                height: MediaQuery.of(context).size.height,
                width: double.infinity,
                child: ListView.builder(
                    itemCount: user.length,
                    itemBuilder: (BuildContext context, int index) {
                      return FlatButton(
                          // elevation: 5,
                          // color: Theme.of(context).accentColor,
                          child: List_Item(index, user),
                          onPressed: () {
                            if (index == 0)
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AdminLoginScreen()),
                              );
                            else if (index == 1)
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => LoginScreen()),
                              );
                            else
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => StudentLoginScreen()),
                              );
                          });
                    }),
              ),
              Container(
                height: 130,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: primary,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30))),
                child: Padding(
                  padding: const EdgeInsets.only(left: 30, right: 30, top: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          "Attendance Management System",
                          style: TextStyle(color: Colors.white, fontSize: 24),
                          maxLines: 2,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // Widget buildList(BuildContext context, int index) {
  //   return Container(
  //     decoration: BoxDecoration(
  //       borderRadius: BorderRadius.circular(25),
  //       color: Colors.white,
  //     ),
  //     width: double.infinity,
  //     height: 110,
  //     margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
  //     padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
  //     child: Row(
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       children: <Widget>[
  //         Container(
  //           padding: EdgeInsets.only(right: 15),
  //           child: Column(
  //             mainAxisAlignment: MainAxisAlignment.center,
  //             crossAxisAlignment: CrossAxisAlignment.center,
  //             children: [
  //               Icon(
  //                 Icons.account_circle,
  //                 color: secondary,
  //                 size: 50,
  //               ),
  //             ],
  //           ),
  //         ),
  //         Expanded(
  //           child: Column(
  //             crossAxisAlignment: CrossAxisAlignment.start,
  //             mainAxisAlignment: MainAxisAlignment.center,
  //             children: <Widget>[
  //               Column(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 children: [
  //                   Text(
  //                     user[index]['name'],
  //                     style: TextStyle(
  //                         color: primary,
  //                         fontWeight: FontWeight.bold,
  //                         fontSize: 18),
  //                   ),
  //                 ],
  //               ),
  //             ],
  //           ),
  //         )
  //       ],
  //     ),
  //   );
  // }
}
