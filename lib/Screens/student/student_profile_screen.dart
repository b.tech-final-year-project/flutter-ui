import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import '../../config.dart';

class StudentProfile extends StatefulWidget {
  static final routeName = '/student-profile-screen';
  @override
  _StudentProfileState createState() => _StudentProfileState();
}

class _StudentProfileState extends State<StudentProfile> {
  bool _isLoadingInitData = true;
  String token;
  String regno;
  bool _isRaisingTicket = false;
  var _issueInputController = TextEditingController();
  String student_name;
  dynamic currentCourse;
  String title = "Student Name";
  int totalClasses = 0;
  int presentCount = 0;
  dynamic presentDates = [];
  dynamic absentDates = [];

  @override
  void didChangeDependencies() async {
    if (_isLoadingInitData) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String stringValueToken = prefs.getString('token');
      String stringValueRegno = prefs.getString('regno');
      String stringValueName = prefs.getString('name');
      setState(() {
        token = stringValueToken;
        regno = stringValueRegno;
        student_name = stringValueName;
      });
      final routeArgs = ModalRoute.of(context).settings.arguments as dynamic;
      setState(() {
        currentCourse = routeArgs["currentCourse"];
        title = currentCourse.name;
        _isLoadingInitData = false;
      });
      loadInitData();
    }
    super.didChangeDependencies();
  }

  void loadInitData() async {
    Uri url = Uri.parse("$baseUrl/attendance/statistics");
    Map<String, String> headers = {
      'x-access-token': token,
      'Accept': 'Application/json',
    };
    MultipartRequest request = MultipartRequest("POST", url);
    request.fields["regno"] = regno;
    request.fields["id"] = currentCourse.id.toString();
    request.headers["x-access-token"] = token;
    try {
      final streamedResponse = await request.send();
      final response = await Response.fromStream(streamedResponse);
      print(response.statusCode);
      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body);
        print(jsonData);
        if (jsonData["Status"] == "Success") {
          setState(() {
            totalClasses = jsonData["total_count"];
            presentCount = jsonData["present_count"];
            presentDates = jsonData["present_dates"];
            absentDates = jsonData["absent_dates"];
          });
        }
      } else {
        print("Error");
      }
    } on Exception catch (_) {
      print('object');
    }
    print(absentDates);
  }

  void raiseTicket() async {
    setState(() {
      _isRaisingTicket = true;
    });

    Uri url = Uri.parse("$baseUrl/student/ticket");

    MultipartRequest request = MultipartRequest("POST", url);
    request.fields["course_id"] = currentCourse.id.toString();
    request.fields["issue"] = _issueInputController.text;
    request.headers["x-access-token"] = token;

    try {
      final streamedResponse = await request.send();
      final response = await Response.fromStream(streamedResponse);
      if (response.statusCode == 200) {
        print("Success");
        var jsonData = jsonDecode(response.body);
        if (jsonData["status"] == "Success") {
          Toast.show(jsonData["message"], context,
              duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
          setState(() {
            _isRaisingTicket = false;
          });
        }
      } else {
        print("Error");
      }
    } on Exception catch (_) {}
  }

  void _raiseTicket(BuildContext ctx) {
    showModalBottomSheet(
      context: ctx,
      builder: (_) {
        return GestureDetector(
          onTap: () {},
          child: Container(
            padding: EdgeInsets.all(16),
            child: Column(
              children: <Widget>[
                TextFormField(
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  maxLength: 1000,
                  controller: _issueInputController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Issue',
                      labelStyle: TextStyle(fontSize: 15)),
                ),
                _isRaisingTicket
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : FlatButton(
                        height: 50,
                        color: Theme.of(context).primaryColor,
                        textColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        onPressed: () {
                          raiseTicket();
                          Navigator.of(context).pop();
                          _issueInputController.clear();
                        },
                        child: Text('Raise Ticket'),
                      )
              ],
            ),
          ),
          behavior: HitTestBehavior.opaque,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: _isLoadingInitData
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(8),
                child: Column(
                  children: <Widget>[
                    Card(
                      elevation: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Name : ${student_name}",
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                          Divider(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Roll No : ${regno}",
                              style: TextStyle(fontSize: 16),
                            ),
                          )
                        ],
                      ),
                    ),
                    Card(
                      elevation: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Center(
                              child: Text(
                                "Attendance Statistics",
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          Divider(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Total Classes : $totalClasses",
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Classes Present: $presentCount",
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Classes Absent: ${totalClasses - presentCount}",
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Card(
                      elevation: 2,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Absent Dates',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Divider(),
                          Container(
                            padding: EdgeInsets.all(8),
                            // height: 40.0 * (totalClasses - presentCount),
                            child: ListView.builder(
                              shrinkWrap: true,
                              itemBuilder: (ctx, index) {
                                return Text(
                                  "${absentDates[index].split('_')[0]} : ${absentDates[index].split('_')[1]}",
                                  style: TextStyle(fontSize: 16),
                                );
                              },
                              itemCount: absentDates.length,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => _raiseTicket(context),
        icon: Icon(Icons.assistant_photo),
        label: Text('Raise Ticket'),
      ),
    );
  }
}
