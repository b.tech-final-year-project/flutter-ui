import 'dart:convert';

import 'package:attendance_app/Models/Course.dart';
import 'package:attendance_app/Screens/student/student_profile_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../../config.dart';
import '../settings_screen.dart';

class StudentHome extends StatefulWidget {
  static final routeName = '/student-home';
  @override
  _StudentHomeState createState() => _StudentHomeState();
}

class _StudentHomeState extends State<StudentHome> {
  List<Course> courseList = [];
  String token;
  dynamic currentCourse;

  var _loadedInitData = false;

  @override
  void didChangeDependencies() async {
    if (_loadedInitData == false) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String stringValue = prefs.getString('token');
      setState(() {
        token = stringValue;
      });
      var response = await http.get(
        Uri.encodeFull(baseUrl + '/student_courses'),
        headers: {
          'x-access-token': token,
          'Accept': 'Application/json',
        },
      );
      var data = jsonDecode(response.body);
      var courses = data['courses'];
      if (courses != null) {
        for (int i = 0; i < courses.length; i++) {
          var course = courses[i];
          var newCourse = new Course(
            id: course['id'],
            code: course['code'],
            name: course['name'],
            instructorId: course['instructor_id'],
            academicSession: course['academic_session'],
          );
          setState(() {
            courseList.add(newCourse);
          });
        }
      }
    }
    super.didChangeDependencies();
  }

  void logout() async {
    final prefrences = await SharedPreferences.getInstance();
    await prefrences.clear();
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    final primary = Theme.of(context).primaryColor;
    return Scaffold(
      appBar: AppBar(
        title: Text("Registered Courses"),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.settings),
              onPressed: () =>
                  Navigator.of(context).pushNamed(SettingsScreen.routeName)),
          IconButton(icon: Icon(Icons.power_settings_new), onPressed: logout)
        ],
      ),
      body: courseList.length == 0
          ? Center(
              child: Text(
                'You\'re not enrolled in any courses',
                style: TextStyle(fontSize: 16, color: Colors.grey),
              ),
            )
          : ListView.builder(
              itemBuilder: (ctx, index) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 8),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).pushNamed(
                            StudentProfile.routeName,
                            arguments: {
                              "currentCourse": courseList[index],
                            },
                          );
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.white,
                          ),
                          width: double.infinity,
                          height: 75,
                          margin:
                              EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                          padding:
                              EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "${courseList[index].code} : ${courseList[index].name}",
                                          style: TextStyle(
                                              color: primary,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              },
              itemCount: courseList.length,
            ),
    );
  }
}
