import 'package:attendance_app/Screens/admin/previous_images.dart';
import 'package:attendance_app/Widgets/list_item.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';

import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:attendance_app/Models/Course.dart';
import 'package:attendance_app/config.dart';

class UploadStudentPhoto extends StatefulWidget {
  static final routeName = '/upload-student-photo';

  @override
  _UploadStudentPhotoState createState() => _UploadStudentPhotoState();
}

class _UploadStudentPhotoState extends State<UploadStudentPhoto> {
  bool _isImageLoaded = false;
  bool _isUpdatingAttendance = false;
  bool _loadedInitData = false;
  dynamic preImages = [];
  bool _isLoadingPrevPics = true;
  String token = '';
  dynamic student_regno;
  dynamic student_name;
  List<DropdownMenuItem<Course>> _dropdownMenuItems = [];
  List<Asset> images = List<Asset>();
  String _error;
  Uri uri = Uri.parse('$baseUrl/students/photo');

  @override
  void didChangeDependencies() async {
    if (_loadedInitData == false) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String stringValue = prefs.getString('token');
      student_regno = prefs.getString('regno');
      student_name = prefs.getString('name');
      setState(() {
        token = stringValue;
      });
      setState(() {
        _loadedInitData = true;
      });
      await loadPreviousPics();
    }
    super.didChangeDependencies();
  }

  Future<void> loadPreviousPics() async {
    var url = "$baseUrl/studentphotos/$student_regno";

    print(url);

    var r = await get(
      Uri.encodeFull(url),
      headers: {
        'x-access-token': token,
        'Accept': 'Application/json',
      },
    );

    if (r.statusCode == 200) {
      var jsonData = jsonDecode(r.body);
      print(r.body);
      setState(() {
        preImages = jsonData["Photo URLs"];
      });
    }

    setState(() {
      _isLoadingPrevPics = false;
    });
  }

  Future<String> _uploadMultipleImage() async {
    setState(() {
      _isUpdatingAttendance = true;
    });
    MultipartRequest request = MultipartRequest("POST", uri);

    var iter = 0;
    var var1 = DateTime.now().toString();
    for (Asset image in images) {
      ByteData byteData = await image.getByteData();
      List<int> imageData = byteData.buffer.asUint8List();

      MultipartFile multipartFile = MultipartFile.fromBytes(
        'file',
        imageData,
        filename: 'img$var1$iter.jpg',
        contentType: MediaType("image", "jpg"),
      );

// add file to multipart
      request.files.add(multipartFile);
      request.fields['regno'] = student_regno;
      print(student_regno);
      request.headers["x-access-token"] = token;
      iter += 1;
    }
// send
    try {
      final streamedResponse = await request.send();
      final response = await Response.fromStream(streamedResponse);
      if (response.statusCode == 200) {
        // sendAlignRequest();
        final responseData = json.decode(response.body);
        print(json.decode(response.body));
        Navigator.of(context).pop();
      } else {
        print(response.statusCode);
      }
    } on Exception catch (_) {
      setState(() {
        _error = "error uploading file";
      });
    }
    setState(() {
      _isUpdatingAttendance = false;
    });
  }

  void sendAlignRequest() async {
    var response = await get(
      Uri.encodeFull(baseUrl + '/align_images'),
      headers: {
        'x-access-token': token,
        'Accept': 'Application/json',
      },
    );
  }

  @override
  void initState() {
    super.initState();
  }

  Widget buildGridView() {
    if (images != null && images.length != null && images.length != 0)
      return GridView.count(
        shrinkWrap: true,
        crossAxisCount: 3,
        children: List.generate(images.length, (index) {
          Asset asset = images[index];
          return AssetThumb(
            asset: asset,
            width: 300,
            height: 300,
          );
        }),
      );
    else
      return Container();
  }

  Future<void> loadAssets() async {
    setState(() {
      images = List<Asset>();
    });

    List<Asset> resultList;
    String error;

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images = resultList;
      if (error == null && images != null) {
        _error = 'No Error Dectected';
        _isImageLoaded = true;
      }
    });
  }

  Widget _printSubmitButton() {
    if (_isImageLoaded) {
      return _isUpdatingAttendance
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              alignment: Alignment.center,
              child: FlatButton(
                height: 50,
                color: Colors.green[300],
                textColor: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                onPressed: _uploadMultipleImage,
                child: Text('Upload Photos'),
              ),
            );
    } else {
      return Container(
        width: 0,
        height: 0,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Upload Photos'),
      ),
      body: !_loadedInitData
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 20,
                ),
                List_Item(
                  0,
                  [
                    {
                      "name": student_name,
                      "icon": Icons.account_circle,
                    },
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                // Center(child: Text('Error: $_error')),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FlatButton(
                      height: 50,
                      color: Color(0xff696b9e),
                      textColor: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        "Select images",
                      ),
                      onPressed: loadAssets,
                    ),
                    FlatButton(
                      height: 50,
                      color: Color(0xff696b9e),
                      textColor: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Text("View Previous images"),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => PreviousImages(),
                            settings: RouteSettings(
                              arguments: preImages,
                            ),
                          ),
                        );
                      },
                    )
                  ],
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.all(10),
                          child: _printSubmitButton()),
                    ]),
                Expanded(
                  child: buildGridView(),
                )
              ],
            ),
    );
  }
}
