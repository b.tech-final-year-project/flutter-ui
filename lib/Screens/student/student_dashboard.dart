import 'package:attendance_app/Screens/student/student_home.dart';
import 'package:attendance_app/Screens/student/student_photos.dart';
import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

import '../../Auth/google_sign_in.dart';
import '../../Widgets/main_drawer.dart';
import '../../Widgets/main_screen_link.dart';
// import 'add_attendance_screen.dart';
import '../settings_screen.dart';

class StudentDashboard extends StatefulWidget {
  static final routeName = '/StudentDashboard';
  @override
  _StudentDashboardState createState() => _StudentDashboardState();
}

class _StudentDashboardState extends State<StudentDashboard> {
  String token = '';
  String currentUser = '';
  var links = [
    {
      'title': "Courses",
      'icon': Icons.filter_1_outlined,
      'route': StudentHome.routeName,
    },
    {
      'title': "Add Photos",
      'icon': Icons.add_circle_outline,
      'route': UploadStudentPhoto.routeName,
    },
  ];

  void logout() async {
    final prefrences = await SharedPreferences.getInstance();
    await prefrences.clear();
    signOutGoogle();
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  void initState() {
    // getCurrentUserData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Attendance App'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.settings),
              onPressed: () =>
                  Navigator.of(context).pushNamed(SettingsScreen.routeName)),
          IconButton(icon: Icon(Icons.power_settings_new), onPressed: logout)
        ],
      ),
      drawer: MainDrawer(currentUser),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return MainScreenLink(links[index]['title'], links[index]['route'],
              links[index]['icon']);
        },
        itemCount: links.length,
      ),
    );
  }
}
