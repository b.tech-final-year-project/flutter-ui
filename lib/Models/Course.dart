class Course {
  int id;
  String code;
  String name;
  int instructorId;
  String academicSession;
  String thresholdValue;

  Course({
    this.id,
    this.code,
    this.name,
    this.instructorId,
    this.academicSession,
    this.thresholdValue,
  });
}
