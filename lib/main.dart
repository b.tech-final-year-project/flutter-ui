import 'package:attendance_app/Screens/admin/add_admin_screen.dart';
import 'package:attendance_app/Screens/admin/admin_dashboard.dart';
import 'package:attendance_app/Screens/admin/manage_admins.dart';
import 'package:attendance_app/Screens/faculty/fac_student_profile_screen.dart';
import 'package:attendance_app/Screens/home.dart';
import 'package:attendance_app/Screens/student/student_home.dart';
import 'package:attendance_app/Screens/student/student_photos.dart';
import 'package:flutter/material.dart';

import 'Screens/admin/upload_photo_screen.dart';
import 'Screens/faculty/fac_view_raised_tickets.dart';
import 'Screens/faculty/known_faces_screen.dart';
import 'Screens/faculty/login_screen.dart';
import 'Screens/faculty/dashboard.dart';
import 'Screens/faculty/create_course_screen.dart';
import 'Screens/faculty/add_attendance_screen.dart';
import 'Screens/faculty/manage_courses_screen.dart';
import 'Screens/faculty/attendance_result_screen.dart';
import 'Screens/faculty/attendance_list_screen.dart';
import 'Screens/faculty/add_attendance_screen_multi.dart';
import 'Screens/faculty/manage_course_screen.dart';
import 'Screens/faculty/enrolled_students_screen.dart';
import 'Screens/faculty/enroll_students_screen.dart';
import 'Screens/admin/add_faculty_screen.dart';
import 'Screens/admin/manage_faculties_screen.dart';
import 'Screens/admin/add_student_screen.dart';
import 'Screens/admin/manage_students_screen.dart';
import 'Screens/faculty/view_previous_attendance_screen.dart';
import 'Screens/admin/fac_view_tickets_screen.dart';
import 'Screens/admin/student_view_tickets_screen.dart';
import 'Screens/admin/view_tickets.dart';
import 'Screens/faculty/view_course_attendance_screen.dart';
import 'Screens/faculty/low_attendance_screen.dart';
import 'Screens/student/student_profile_screen.dart';
import 'Screens/student/student_dashboard.dart';
import 'Screens/admin/manage_facutly_screen.dart';
import 'Screens/admin/admin_login_screen.dart';
import 'Screens/faculty/unknown_faces_screen.dart';
import 'Screens/settings_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  MaterialColor createMaterialColor(Color color) {
    List strengths = <double>[.05];
    Map swatch = <int, Color>{};
    final int r = color.red, g = color.green, b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    strengths.forEach((strength) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    });
    return MaterialColor(color.value, swatch);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Attendance App',
      theme: ThemeData(
        primarySwatch: createMaterialColor(Color(0xFF6F35A5)),
        textTheme: ThemeData.light().textTheme.copyWith(),
        scaffoldBackgroundColor: const Color(0xfff0f0f0),
      ),
      // home: LoginScreen(),
      // home: Dashboard(),
      routes: {
        '/': (ctx) => Home(),
        Home.routeName: (ctx) => Home(),
        LoginScreen.routeName: (ctx) => LoginScreen(),
        AdminLoginScreen.routeName: (ctx) => AdminLoginScreen(),
        AdminDashboard.routeName: (ctx) => AdminDashboard(),
        Dashboard.routeName: (ctx) => Dashboard(),
        CreateCourseScreen.routeName: (ctx) => CreateCourseScreen(),
        AddAttendanceScreen.routeName: (ctx) => AddAttendanceScreen(),
        ManageCoursesScreen.routeName: (ctx) => ManageCoursesScreen(),
        AttendanceResultScreen.routeName: (ctx) => AttendanceResultScreen(),
        AttendanceListScreen.routeName: (ctx) => AttendanceListScreen(),
        AddAttendanceScreenMulti.routeName: (ctx) => AddAttendanceScreenMulti(),
        ManageCourseScreen.routeName: (ctx) => ManageCourseScreen(),
        EnrolledStudetsScreen.routeName: (ctx) => EnrolledStudetsScreen(),
        EnrollStudentsScreen.routeName: (ctx) => EnrollStudentsScreen(),
        AddFaculty.routeName: (ctx) => AddFaculty(),
        ManageFaculties.routeName: (ctx) => ManageFaculties(),
        AddAdmin.routeName: (ctx) => AddAdmin(),
        StudentHome.routeName: (ctx) => StudentHome(),
        StudentDashboard.routeName: (ctx) => StudentDashboard(),
        ManageAdmins.routeName: (ctx) => ManageAdmins(),
        AddStudent.routeName: (ctx) => AddStudent(),
        ManageStudents.routeName: (ctx) => ManageStudents(),
        PreviousAttendance.routeName: (ctx) => PreviousAttendance(),
        ViewTickets.routeName: (ctx) => ViewTickets(),
        ViewFacTickets.routeName: (ctx) => ViewFacTickets(),
        StudentViewTickets.routeName: (ctx) => StudentViewTickets(),
        FacViewTickets.routeName: (ctx) => FacViewTickets(),
        CourseAttendance.routeName: (ctx) => CourseAttendance(),
        LowAttendanceScreen.routeName: (ctx) => LowAttendanceScreen(),
        StudentProfile.routeName: (ctx) => StudentProfile(),
        FacultyStudentProfile.routeName: (ctx) => FacultyStudentProfile(),
        UploadPhotoStudent.routeName: (ctx) => UploadPhotoStudent(),
        UploadStudentPhoto.routeName: (ctx) => UploadStudentPhoto(),
        ManageFaculty.routeName: (ctx) => ManageFaculty(),
        UnknownFaces.routeName: (ctx) => UnknownFaces(),
        KnownFaces.routeName: (ctx) => KnownFaces(),
        SettingsScreen.routeName: (ctx) => SettingsScreen(),
      },
      debugShowCheckedModeBanner: false,
    );
  }
}
